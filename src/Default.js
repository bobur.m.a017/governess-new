import React from 'react';
import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

export const baseUrl = () => {
    // return "http://185.217.131.189:8888/out/api/attachment/";
    return "http://192.168.136.204:8888/out/api/attachment/";
}

export const baseUrl3 = () => {
    // return "http://my.governess.uz";
    return "http://localhost:3000"
}

export const baseUrl2 = () => {
    // return "http://185.217.131.189:8888/out/api";
    return "http://192.168.136.204:8888/out/api";
    // return "http://localhost:8888/out/api";
}

export const pushLogin = () => {
    localStorage.setItem("Authorization","");
    localStorage.setItem("Refresh","");
    window.history.pushState("object or string", "Title", "/");
    window.location.reload();
}

function Default(props) {
    const [state, setState] = useState();
    const dispatch = useDispatch();
    const stateSelector = useSelector(state => state);
    const firstUpdate = useRef(false);

    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
        }
    }, []);

    return (
        <div></div>
    );
}

export default Default;
