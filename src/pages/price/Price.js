import React from 'react';
import NavbarHeader from "../more/NavbarHeader";
import {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Col, Container, Form, InputGroup, Modal, Row} from "react-bootstrap";
import {addPrice, byProductPrice, editPrice, getPriceAll} from "./PriceReducer";
import {TimestampToInputDate, TimestampToInputMonth} from "../funcs/Funcs";
import {BsThreeDotsVertical} from "react-icons/bs";
import FromPageSizeBottom from "../fromPage/FromPageSizeBottom";
import {toast} from "react-toastify";
import {getDepartment} from "../departments/RegionDepartmentReducer";
import {getByDepartmentMtt} from "../mtt/MttReducer";
import DistrictStyle from "../more/DistrictStyle";

function Price() {
    const defaultObj = {
        "districtId": '',
        "month": '',
        "price": '',
        "productId": '',
        "year": ""
    };
    const [date, setDate] = useState({month: '', year: ''});
    const [activeMore, setActiveMore] = useState();
    const [district, setDistrict] = useState();
    const [params, setParams] = useState();
    const [oneProduct, setOneProduct] = useState();
    const [productState, setProductState] = useState(defaultObj);
    const [pricesState, setPricesState] = useState();
    const prices = useSelector(state => state.price.prices)
    const pricesByProduct = useSelector(state => state.price.pricesByProduct)
    const departments = useSelector(state => state.department.departments)
    const result = useSelector(state => state.price.result);
    const dispatch = useDispatch();
    const firstUpdate = useRef(false);
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = (num, data) => {
        if (num === 1) {
            setProductState(defaultObj);
        } else if (num === 2) {
            setProductState(data);
        }
        if (oneProduct?.id) {
            setShow(true)
        }
    };

    useEffect(() => {
        if (!firstUpdate.current) {

        } else {
            dispatch(getPriceAll(params));
            handleClose();
        }
    }, [result]);


    useEffect(() => {
        if (!firstUpdate.current) {

        } else {
            setPricesState(prices);
        }
    }, [prices]);

    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            dispatch(getDepartment());
        }
    }, []);

    const getPricesByProduct = (data) => {
        setOneProduct({
            ...oneProduct,
            id:data.id,
            productId: data.productId,
            price: data.price,
            month: date?.month,
            year: date?.year,
            districtId: params?.districtId,
            name:data?.name
        })
        setShow(true);
    }
    const onChangeProductPrice = (e) => {
        setOneProduct({...oneProduct, [e.target.name]: e.target.value})
    }

    const getMtts = (data) => {
        setParams({...params, districtId: data.id, month: date.month, year: date.year})
        dispatch(getPriceAll({districtId: data.id, month: date.month, year: date.year}))
    }


    const submitFunc = (e) => {
        e.preventDefault();
        if (date.month) {
            if (oneProduct?.id) {
                dispatch(editPrice(oneProduct));
            } else {
                dispatch(addPrice(oneProduct));
            }
            handleClose();
        } else {
            toast.error("Sanani tanlang!");
        }
    }

    const changeDate = (e) => {
        let year = new Date(e.target.value).getFullYear();
        let month = (new Date(e.target.value).getMonth() + 1);
        setDate({...date, month, year, date: new Date(e.target.value).getTime()});
    }
    const getProduct = (pageNumber) => {
        dispatch(getPriceAll({...params, pageNumber}));
        setParams({...params, pageNumber})
    }
    return (
        <div className={"allMain"}>
            <NavbarHeader name={"Mahsulot narxlari"}/>
            <div className={"figma-card w-25 mt-3"}>
                <Form.Label>Sana</Form.Label>
                <Form.Control type="month" value={TimestampToInputMonth(date?.date)} onChange={changeDate}/>
            </div>
            <Row className={'mt-3'}>
                <Col xs={12} sm={12} md={4} lg={3} xl={3}>
                    <div className={"figma-card d-flex justify-content-around"}>
                        <DistrictStyle list={departments} getData={getMtts}/>
                    </div>
                </Col>
                <Col xs={12} sm={12} md={8} lg={9} xl={9}>
                    <div className={'figma-card'}>
                        <div className={'tableCalendar'}>
                            <table>
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Nomi</th>
                                    <th>Narx</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    pricesState?.list?.map((product, index) =>
                                        <tr key={index} onClick={() => getPricesByProduct(product)}
                                            style={{cursor: 'pointer'}}>
                                            <td>{index + 1}</td>
                                            <td>{product.name}</td>
                                            <td>{product.price}</td>
                                        </tr>
                                    )
                                }
                                </tbody>
                            </table>
                            <br/>
                            <FromPageSizeBottom allPageSize={pricesState?.allPageSize} pageSize={pricesState?.pageSize}
                                                currentPage={pricesState?.getPageNumber} changesPage={getProduct}/>
                        </div>

                    </div>
                </Col>
            </Row>

            <Modal show={show} onHide={handleClose}>
                <Form onSubmit={submitFunc}>
                    <Modal.Header closeButton>
                        <Modal.Title>{oneProduct?.name}ga narx kiritish</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <InputGroup className="mb-3">
                            <InputGroup.Text> Narxni kiriting</InputGroup.Text>
                            <Form.Control type={'number'} required value={oneProduct?.price}
                                          onWheel={e => e.target.blur()}
                                          name={'price'} onChange={onChangeProductPrice} placeholder={"Narx "}/>
                        </InputGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={handleClose}>
                            Bekor qilish
                        </Button>
                        <Button variant="primary" type={'submit'}>
                            Tayyor
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </div>
    );
}

export default Price;
