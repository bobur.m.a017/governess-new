import React from 'react';
import {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Col, Form, Row} from "react-bootstrap";
import NavbarHeader from "../more/NavbarHeader";
import {TimestampToInputDate} from "../funcs/Funcs";
import {getDepartment} from "../departments/RegionDepartmentReducer";
import DistrictStyle from "../more/DistrictStyle";
import CheckBoxCustom2 from "../more/CheckBoxCustom2";
import {getByDepartmentMtt} from "../mtt/MttReducer";
import {getAllMenu, getReport} from "./ReportReducer";
import LoadingPage from "../loading/LoadingPage";
import axios from "axios";
import GetKinderByDepartment from "../GetKinderByDepartment";


function Report() {

    const FileDownload = require('js-file-download');
    const defaParams = {
        ustama: '',
        startDate: '',
        endDate: '',
        districtId: null,
        kindergartenId:''
    };
    const [mttState, setMttState] = useState([]);
    const [depar, setDepar] = useState();
    const [navs, setNavs] = useState(0);
    const [load, setLoad] = useState(false);
    const [params, setParams] = useState(defaParams);
    const firstUpdate = useRef(false);
    const departments = useSelector(state => state.department.departments);
    const mtts = useSelector(state => state.mtt.mtts);
    const reportsResult = useSelector(state => state.report.reportsResult);
    const error = useSelector(state => state.report.error);
    const dispatch = useDispatch();

    useEffect(() => {
        if (firstUpdate.current) {
            if (reportsResult) {
                axios({
                    url: `http://${reportsResult}`,
                    method: 'GET',
                    responseType: 'blob', // Important
                }).then((response) => {
                    // setFileType(response.data);
                    // console.log(response.data)
                    FileDownload(response.data, 'hisobot.xlsx');
                });
            }
        }
        setLoad(false);
        // let win = window.open(`http://${action.payload}`, '_blank');
        // win.focus();
    }, [reportsResult, error])


    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            dispatch(getDepartment());
        } else {
            setMttState(mtts);
        }
    }, [mtts])

    const submitInoutGet = (e) => {
        e.preventDefault();
        dispatch(getReport(params, mttState?.filter(item => item.checked === true).map(item => item.id)));
        setLoad(true);
    }
    const getDepartmentData = (data) => {
        dispatch(getByDepartmentMtt(data.id));
        setParams({...params, districtId: data.id});
        setDepar(data.name);
    }
    const getChecked = (data, checked, index) => {
        let ageGroupList = [...mttState];
        ageGroupList[index] = {...ageGroupList[index], checked};
        setMttState([...ageGroupList]);
    }
    const allChecked = (checked) => {
        setMttState(mttState?.map((item) => {
            return {...item, checked}
        }));
    }

    const menyuAll = (e) => {
        e.preventDefault();
        if (params.kindergartenId) {
            dispatch(getAllMenu({startDate: params.startDate,endDate: params.endDate},params.kindergartenId));
        }
    }
    const getDepartmentId = (data) => {

    }
    const getKinderId = (data) => {
        console.log(data)
        setParams({...params,kindergartenId:data.id})
    }
    return (
        <div className={'allMain'}>
            <NavbarHeader name={"Hisobotlar"} navs={[{name: "Hisobotlar"}, {name: "MTT menyulari"}]}
                          currentNavs={setNavs}/>
            <br/>
            {navs === 0 ? < div className={"figma-card"}>
                <div>
                    <Form onSubmit={submitInoutGet} className={"w-100"}>
                        <Row className={"w-100"}>
                            <Col md={3}>
                                <Form.Label className={"d-flex"}>Boshlanish sana</Form.Label>
                                <Form.Control type={'date'} name={"startDate"}
                                              value={TimestampToInputDate(params.startDate)}
                                              required={true}
                                              onChange={(e) => setParams({
                                                  ...params,
                                                  [e.target.name]: new Date(e.target.value).getTime()
                                              })}/>
                            </Col>
                            <Col md={3}>
                                <Form.Label className={"d-flex"}>Tugash sana</Form.Label>
                                <Form.Control type={'date'} name={"endDate"}
                                              value={TimestampToInputDate(params.endDate)}
                                              min={TimestampToInputDate(params.startDate)}
                                              onChange={(e) => setParams({
                                                  ...params,
                                                  [e.target.name]: new Date(e.target.value).getTime()
                                              })}
                                              required={true}
                                />
                            </Col>
                            <Col md={3}>
                                <Form.Label className={"d-flex"}>Ustama haq</Form.Label>
                                <Form.Control type={'number'} name={"ustama"}
                                              value={params.ustama}
                                              onChange={(e) => setParams({
                                                  ...params,
                                                  [e.target.name]: e.target.value
                                              })}
                                              required={true}
                                />
                            </Col>
                            <Col md={2}>

                                <div className={'w-100 d-flex justify-content-between mt-2'}>
                                    <button className={"createButtons mt-4 mx-2"} type={"submit"}>TAYYOR</button>
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </div>
                <br/>
                <Row>
                    <Col>
                        <br/>
                        <br/>
                        <br/>
                        <DistrictStyle list={departments} getData={getDepartmentData}/>
                    </Col>
                    <Col>
                        <CheckBoxCustom2 getChecked={getChecked} name={"name"} list={mttState} allChecked={allChecked}/>
                    </Col>
                </Row>
            </div> : null}
            {navs === 1 ? <div className={"figma-card"}>
                <div className={"d-flex justify-content-around"}>
                    <GetKinderByDepartment getDepartmentId={getDepartmentId} getKinderId={getKinderId}/>
                    <div className={"text-center d-flex justify-content-center px-2"}>
                        <Form onSubmit={menyuAll} className={"w-100"}>
                            <Row className={"w-100 d-flex justify-content-around"}>
                                <Col>
                                    <Form.Label className={"d-flex"}>Boshlanish sana</Form.Label>
                                    <Form.Control type={'date'} name={"startDate"}
                                                  value={TimestampToInputDate(params.startDate)}
                                                  required={true}
                                                  onChange={(e) => setParams({
                                                      ...params,
                                                      [e.target.name]: new Date(e.target.value).getTime()
                                                  })}/>
                                </Col>
                                <Col>
                                    <Form.Label className={"d-flex"}>Tugash sana</Form.Label>
                                    <Form.Control type={'date'} name={"endDate"}
                                                  value={TimestampToInputDate(params.endDate)}
                                                  min={TimestampToInputDate(params.startDate)}
                                                  onChange={(e) => setParams({
                                                      ...params,
                                                      [e.target.name]: new Date(e.target.value).getTime()
                                                  })}
                                                  required={true}
                                    />
                                </Col>
                                <Col>
                                    <br/>
                                    <button className={"buttonPdf mt-2"} type={"submit"}>Menyular
                                    </button>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                </div>

            </div> : null}
            <LoadingPage load={load}/>
        </div>
    );
}

export default Report;
