import React from 'react';
import {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useNavigate, useParams} from "react-router-dom";
import {getMenuOne} from "../multimenu/MultiMenuReducer";
import {Button, Card, Col, Container, Form, Modal, Row} from "react-bootstrap";
import {tableRowCustomTd3} from "../more/Functions";
import {getProductByMeal} from "../product/ProductReducer";
import {baseUrl} from "../../Default";

function GetOneDayMenu() {
    const [meal, setMeal] = useState({date: undefined});
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = (data) => {
        setMeal(data);
        setShow(true);
    };
    const dispatch = useDispatch();
    const history = useNavigate();
    const stateSelector = useSelector(state => state.multiMenu.menu);
    const product = useSelector(state => state.product.productMeal);
    const firstUpdate = useRef(false);
    const reportId = useParams("id");

    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            dispatch(getMenuOne(reportId?.id))
        } else {
            console.log(product, "stateSelector");
        }
    }, [stateSelector, product]);

    const productByMeal = (e) => {
        dispatch(getProductByMeal({date: new Date(e.target.value).getTime()}, meal?.id));
    }

    return (
        <div>
            <div className={'figma-card'}>
                <button className={"buttonPdf"} onClick={() => history("/sidebar/menu-mtt")}>ORTGA</button>
                <Col xs={12} sm={12} md={12}
                     className={'text-center mt-3'}>
                    <Card className={'m-0'}>
                        <Card.Header
                            style={{backgroundColor: '#d7d6d6'}}>{stateSelector?.multiMenuName}sini</Card.Header>
                        <Card.Header style={{backgroundColor: '#d7d6d6'}}>{stateSelector.name}</Card.Header>
                        <Container fluid>
                            <Row className={'justify-content-center text-center'}>
                                {
                                    stateSelector?.mealTimeStandardList?.map((mealTime, index2) =>
                                        <Col key={index2} xs={12} sm={12} md={6} lg={6} xl={6}>
                                            <div
                                                className={'w-100 d-flex justify-content-between my-2 fw-bolder'}>{mealTime.mealTimeName}
                                            </div>
                                            <div className={'miniTable1'}>
                                                <table>
                                                    <thead>
                                                    <tr style={{fontSize: 20}}>
                                                        <th>#</th>
                                                        <th>Taom nomi</th>
                                                        <th>Yosh toifalari</th>
                                                        <th>Miqdori</th>
                                                    </tr>
                                                    </thead>
                                                    {mealTime?.mealAgeStandardList?.map((meal, index3) => {
                                                            return (
                                                                <tbody key={index3}>
                                                                <tr style={{fontSize: 20}} onClick={() => handleShow(meal)}>
                                                                    <td rowSpan={meal.ageStandardList.length}>{index3 + 1}</td>
                                                                    <td rowSpan={meal.ageStandardList.length}>{meal.mealName}</td>
                                                                    <td>
                                                                        {
                                                                            meal?.ageStandardList[0].ageGroupName
                                                                        }
                                                                    </td>
                                                                    <td>
                                                                        {meal?.ageStandardList[0].weight}
                                                                    </td>

                                                                </tr>
                                                                {
                                                                    // tableRowCustomTd3(meal?.ageStandardList)
                                                                    meal?.ageStandardList?.map((item, index) => {
                                                                            if (index !== 0) {
                                                                                return (
                                                                                    <tr key={item.id}>
                                                                                        <td>{item.ageGroupName}</td>
                                                                                        <td>{item.weight}</td>
                                                                                    </tr>
                                                                                );
                                                                            }
                                                                        }
                                                                    )
                                                                }
                                                                </tbody>
                                                            )
                                                        }
                                                    )}
                                                </table>
                                            </div>
                                        </Col>
                                    )
                                }
                            </Row>
                        </Container>
                    </Card>
                </Col>
            </div>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        <Form>
                            <Form.Label>Bola soni kiritilgan sanani tanlang</Form.Label>
                            <Form.Control onChange={productByMeal} type={"date"} name={"date"}/>
                        </Form>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <img src={baseUrl() + meal?.mealId} alt=""/>
                    <div className={"tableCalendar"}>
                        <table>
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Mahsulot nomi</th>
                                <th>Miqdori</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                product?.map((item, index) =>
                                    <tr key={index}>
                                        <td>{index + 1}</td>
                                        <td>{item.productName}</td>
                                        <td>{item?.multiply}</td>
                                    </tr>
                                )
                            }

                            </tbody>
                        </table>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleClose}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default GetOneDayMenu;