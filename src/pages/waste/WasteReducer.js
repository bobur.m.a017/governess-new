import {createSlice} from "@reduxjs/toolkit";
import {toast} from "react-toastify";
import {apiCall} from "../../ApiCall";
import {getToken, toastError} from "../more/Functions";

const slice = createSlice({
    name: "waste",
    initialState: {
        result: {},
        error: {},
        wastes: [],
    },
    reducers: {
        wastes: (state, action) => {
            state.wastes = action.payload;
        },
        resultReducer: (state, action) => {
            state.result = action.payload;
            toast.success(action.payload?.text);
        },
        errorReducer: (state, action) => {
            state.error = action.payload;
            toastError(action.payload)
        },
    }
})

export const getWaste = (params) => apiCall({
    url: "/waste",
    method: "GET",
    headers: {
        Authorization: getToken(),
    },
    params,
    success: slice.actions.wastes.type,
    error: slice.actions.errorReducer.type
})

export const deleteWaste = (data) => apiCall({
    url: "/waste/" + data.id,
    method: "DELETE",
    headers: {
        Authorization: getToken(),
    },
    success: slice.actions.resultReducer.type,
    error: slice.actions.errorReducer.type
})


export const addWasteFromWarehouse = (params) => apiCall({
    url: "/waste",
    method: "POST",
    headers: {
        Authorization: getToken(),
    },
    params,
    success: slice.actions.resultReducer.type,
    error: slice.actions.errorReducer.type
})

export const addWasteWarehouse = (id,params) => apiCall({
    url: "/waste/kindergarten/"+id,
    method: "POST",
    headers: {
        Authorization: getToken(),
    },
    params,
    success: slice.actions.resultReducer.type,
    error: slice.actions.errorReducer.type
})

export const addWasteVerified = (id,params) => apiCall({
    url: "/waste/"+id,
    method: "POST",
    headers: {
        Authorization: getToken(),
    },
    params,
    success: slice.actions.resultReducer.type,
    error: slice.actions.errorReducer.type
})

export const editWaste = (data) => apiCall({
    url: "/waste/" + data.id,
    method: "PUT",
    headers: {
        Authorization: getToken(),
    },
    data,
    success: slice.actions.resultReducer.type,
    error: slice.actions.errorReducer.type
})


export default slice.reducer;