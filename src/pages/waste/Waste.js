import React from 'react';
import {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {addWasteVerified, deleteWaste, editWaste, getWaste} from "./WasteReducer";
import {Button, Col, Form, Modal, Row, Table} from "react-bootstrap";
import NavbarHeader from "../more/NavbarHeader";
import {getRoleStorage} from "../more/Functions";
import GetKinderByDepartment from "../GetKinderByDepartment";
import FromPageSizeBottom from "../fromPage/FromPageSizeBottom";
import KindergartenStyle from "../more/KindergartenStyle";


function Waste() {
    const defParam = {districtId: '', kattaOmbor: false, kindergartenId: '', pageNumber: 0, pageSize: 20}
    const [show, setShow] = useState(false);
    const [wasteState, setWasteState] = useState({id: '', name: ''});
    const [wastes, setWastes] = useState([]);
    const [param, setParam] = useState(defParam);
    const handleClose = () => {
        setShow(false);
        setWasteState({id: '', name: ''});
    };
    const handleShow = () => {
        setShow(true)
    };


    const dispatch = useDispatch();
    const firstUpdate = useRef(false);
    const waste = useSelector(state => state.waste)


    useEffect(() => {
        if (firstUpdate.current) {
            dispatch(getWaste(param));
            handleClose();
        }
    }, [waste.result]);

    useEffect(() => {
        setWastes(waste.wastes);
    }, [waste.wastes]);

    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            dispatch(getWaste());
        }
    }, [])

    const submitWaste = (e) => {
        e.preventDefault();
    }
    const onClickDepartment = (data, number) => {
        if (number === 1) {
            dispatch(addWasteVerified(data?.id, {state: true}));
        } else if (number === 2) {
            dispatch(deleteWaste(data));
        } else if (number === 5) {
            dispatch(addWasteVerified(data?.id, {state: false}));
        }
    }

    const getDepartmebtId = (data) => {
        setParam({...param, districtId: data.id, kattaOmbor: false});
        dispatch(getWaste({...param, districtId: data.id, kindergartenId: '', kattaOmbor: false}));
    }
    const getKidndergartenId = (data) => {
        dispatch(getWaste({...param, kindergartenId: data.id, kattaOmbor: false}));
        setParam({...param, kindergartenId: data.id, kattaOmbor: false});
    }
    const onChanges = (param) => (e) => {
        setWasteState({...wasteState, [param]: e.target.value});
    }
    const nextPage = (pageNumber) => (e) => {
        dispatch(getWaste({...param, pageNumber}));
        setParam({...param, pageNumber});
    }
    const getDepo = (data) => {
        dispatch(getWaste({...param, kattaOmbor: true}));
        setParam({...param, kattaOmbor: true});
    }

    return (
        <div className={'allMain'}>
            {/*<NavbarHeader name={"Yosh toifalari bo'limi"} handleShow={handleShow}*/}
            {/*              buttonName={"Yosh toifasini_qo'shish"}/>*/}
            <br/>
            <Row>
                {getRoleStorage() === "ROLE_BO`LIM_BUXGALTER" ? <Col>
                    <KindergartenStyle list={[{id: 0, name: "Asosiy ombor"}]} getData={getDepo}/>
                    <GetKinderByDepartment getDepartmentId={getDepartmebtId} getKinderId={getKidndergartenId}/>
                </Col> : null}
                <Col>
                    <div className={'figma-card'}>
                        <div className={'tableCalendar'}>
                            <table style={{color: 'black'}}>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nomi</th>
                                    <th>Tomonidan</th>
                                    <th>Vazni</th>
                                    <th>Holati</th>
                                    {getRoleStorage() === "ROLE_BO`LIM_BUXGALTER" ? <th>Tasdiqlash</th> : null}
                                    {getRoleStorage() === "ROLE_BO`LIM_BUXGALTER" ? <th>Rad etish</th> : null}
                                    {getRoleStorage() === "ROLE_OMBOR_MUDIRI" || getRoleStorage() === "ROLE_OMBORCHI" ?
                                        <th>O'chirish</th> : null}
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    wastes?.list?.map((item, index) =>
                                        <tr key={index}>
                                            <td>{index + 1}</td>
                                            <td>{item.productName}</td>
                                            <td>{item?.departmentName}</td>
                                            <td>{item.weight}</td>
                                            <td>{item.status}</td>

                                            {getRoleStorage() === "ROLE_BO`LIM_BUXGALTER" ? <td>
                                                <Button variant='outline-info' size='sm'
                                                        onClick={() => onClickDepartment(item, 1)}
                                                        disabled={!(getRoleStorage() === "ROLE_BO`LIM_BUXGALTER" && item.status === "YANGI")}>
                                                    Tasdiqlash
                                                </Button>
                                            </td> : null}

                                            {getRoleStorage() === "ROLE_BO`LIM_BUXGALTER" ? <td>
                                                <Button variant='outline-danger' size='sm'
                                                        onClick={() => onClickDepartment(item, 5)}
                                                        disabled={!(getRoleStorage() === "ROLE_BO`LIM_BUXGALTER" && item.status === "YANGI")}
                                                >
                                                    Rad etish
                                                </Button>
                                            </td> : null}
                                            {getRoleStorage() === "ROLE_OMBOR_MUDIRI" || getRoleStorage() === "ROLE_OMBORCHI" ?
                                                <td>
                                                    <Button variant='outline-danger' size='sm'
                                                            onClick={() => onClickDepartment(item, 2)}
                                                            disabled={!((getRoleStorage() === "ROLE_OMBOR_MUDIRI" || getRoleStorage() === "ROLE_OMBORCHI") && item.status === "YANGI")}
                                                    >
                                                        O'chirish
                                                    </Button>
                                                </td> : null}
                                        </tr>
                                    )
                                }
                                </tbody>
                            </table>
                            <br/>
                            <FromPageSizeBottom allPageSize={wastes?.allPageSize} changesPage={nextPage}
                                                currentPage={wastes?.getPageNumber}/>
                        </div>
                    </div>
                </Col>
            </Row>
            <Modal show={show} onHide={handleClose}>
                <Form onSubmit={submitWaste}>
                    <Modal.Header closeButton>
                        <Modal.Title>{wasteState.name}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Control name='name' required value={wasteState.name} onChange={onChanges("name")}
                                      placeholder="Nomi "/>
                        <br/>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={handleClose}>
                            Ortga
                        </Button>
                        <Button variant="primary" type='submit'>
                            Tayyor
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </div>
    );
}

export default Waste;
