import React from 'react';
import Sidebar from "../components/Sidebar";
import {Link, Route, Routes} from "react-router-dom";
import About from "./About";
import Product from "../product/Product";
import {Container, Nav, Navbar, Row} from "react-bootstrap";
import userLogo from "../image/img.png"
import RegionDepartment from "../departments/RegionDepartment";
import Age from "../age/Age";
import Department from "../departments/Department";
import Mtt from "../mtt/Mtt";
import Meal from "../meal/Meal";
import MultiMenu from "../multimenu/MultiMenu";
import MultiMenuOne from "../multimenu/MultiMenuOne";
import {TbBell} from "react-icons/tb";
import {BiMessageDetail} from "react-icons/bi";
import '../allStyle.scss';
import '../allStyle2.scss';
import Users from "../users/Users";
import MenuView from "../relationMultiMenu/MenuView";
import UsersDepartment from "../users/UsersDepartment";
import RelationMenu from "../relationMultiMenu/RelationMenu";
import Supplier from "../supplier/Supplier";
import Price from "../price/Price";
import Contract from "../contract/Contract";
import CreateContract from "../contract/CreateContract";
import EditContract from "../contract/EditContract";
import {useMediaQuery} from "react-responsive";
import ChildrenNumber from "../children-number/ChildrenNumber";
import Warehouse from "../warehouse/Warehouse";
import GetOneDayMenu from "../report/GetOneDayMenu";
import OneDayMenu from "../multimenu/OneDayMenu";
import OneDayWithMttFromUsers from "../report/OneDayWithMttFromUsers";
import InputOutput from "../report/InputOutput";
import InputOutputFromAdmin from "../report/InputOutputFromAdmin";
import InputOutputKidsNumber from "../report/InputOutputKidsNumber";
import InputOutputKidsNumberFromAdmin from "../report/InputOutputKidsNumberFromAdmin";
import UserInfos from "../users/UserInfos";
import {useDispatch, useSelector} from "react-redux";
import WareHousProductByKinderGarten from "../warehouse/WareHousProductByKinderGarten";
import Notification from "../notification/Notification";
import SendNotifications from "../notification/SendNotifications";
import ProductPack from "../productPack/ProductPack";
import PermissionsFromRelation from "../permission/PermissionsFromRelation";
import MultiMenuOneFromOther from "../multimenu/MultiMenuOneFromOther";
import {getUserData} from "../users/UserReducer";
import {useEffect, useRef} from "react";
import DefaultKidsNumber from "../children-number/DefaultKidsNumber";
import KidsNumberOther from "../children-number/KidsNumberOther";
import logo from "../login/image/img.png"
import logo2 from "../login/image/logo.png"
import OneOrder from "../order/OneOrder";
import Order from "../order/Order";
import Warehouser from "../warehouse/Warehouser";
import Report from "../report/Report";
import Waste from "../waste/Waste";

function Sidebars() {
    const user = useSelector(state => state.user.userData)
    const isBigScreen3 = useMediaQuery({query: '(min-width: 1090px)'});
    const isBigScreen2 = useMediaQuery({query: '(min-width: 576px)'});
    const isBigScreen = useMediaQuery({query: '(max-width: 576px)'});
    const dispatch = useDispatch();
    const firstUpdate = useRef(false);
    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            dispatch(getUserData());
        }
    }, []);

    return (
        <div className={'h-100'}>
            <Navbar bg="light" style={{height: '10%'}}>
                {!isBigScreen ? <Container fluid={true}>
                        <div className={'d-flex justify-content-between w-100'}>
                            <div className={'d-flex '}>
                                <Navbar.Brand>
                                    <img src={logo} alt="logo" height={30} style={{color:'#17da37'}}/>
                                    <img src={logo2} alt="logo" height={30} style={{color:'#17da37',marginLeft:20}}/>
                                </Navbar.Brand>
                                <Nav className="me-auto">
                                    <Nav.Link>
                            <span style={{fontSize: isBigScreen3 ? 30 : !isBigScreen2 ? 25 : 20}}
                                  className={'fw-bolder'}>
                            </span>
                                    </Nav.Link>
                                </Nav>
                            </div>
                            <div className={"d-flex w-25 align-items-center justify-content-around"}>
                                <div className={'p-2'}
                                     style={{backgroundColor: '#eeeeee', borderRadius: "50%", cursor: 'pointer',position:'relative'}}>
                                    {user?.notification !== 0 ? <span style={{
                                        paddingLeft: 2,
                                        paddingRight: 2,
                                        backgroundColor: '#f1a603',
                                        borderRadius: 30,
                                        position: 'absolute',
                                        top: -3,
                                        left: -3
                                    }}>{user?.notification}</span>:null}
                                    <Link
                                    to={"/sidebar/notification"} className={"link-none"}><TbBell size={25}/></Link></div>
                                <div className={'p-2'}
                                     style={{backgroundColor: '#eeeeee', borderRadius: "50%", cursor: 'pointer'}}>
                                    <BiMessageDetail size={25}/></div>
                                <div className={'d-flex justify-content-between align-items-center'}>
                                    <div className={'px-2'}>{user?.name?.substring(0, 1)}.{user?.surname}</div>
                                    <img src={userLogo} alt="" width={35}/>
                                </div>
                            </div>
                        </div>
                    </Container> :
                    <Container fluid={true}>
                        <div className={'w-100 d-flex justify-content-between align-items-center'}>
                            <div className={'d-flex'}>
                                <span style={{fontSize: 12}} className={'fw-bolder mx-2'}>
                            </span>
                            </div>
                            <div className={"d-flex align-items-center justify-content-around w-75"}>
                                <div className={'px-1'}
                                     style={{backgroundColor: '#eeeeee', borderRadius: "30%", cursor: 'pointer'}}>
                                    <Link
                                        to={"/sidebar/notification"} className={"link-none"}>
                                    <span className={"fs-3"}>{user?.notification}</span>
                                        <TbBell
                                            size={15}/></Link></div>
                                <div className={'px-1'}
                                     style={{backgroundColor: '#eeeeee', borderRadius: "30%", cursor: 'pointer'}}>
                                    <BiMessageDetail size={15}/></div>
                                <div className={'d-flex justify-content-between align-items-center'}>
                                    <div className={'px-2'}
                                         style={{fontSize: 10}}>{user?.name?.substring(0, 1)}.{user?.surname}</div>
                                    <img src={userLogo} alt="" width={15}/>
                                </div>
                            </div>
                        </div>
                    </Container>}
            </Navbar>
            <Sidebar>
                <Routes>
                    <Route path="/user" element={<Users/>}/>
                    <Route path="/users-department" element={<UsersDepartment/>}/>
                    <Route path="/info" element={<UserInfos/>}/>
                    <Route path="/mtt" element={<Mtt/>}/>
                    <Route path="/meal" element={<Meal/>}/>
                    <Route path="/product" element={<Product/>}/>
                    <Route path="/age" element={<Age/>}/>
                    <Route path="/admin" element={<About/>}/>
                    <Route path="/super-admin" element={<About/>}/>
                    <Route path="/region-department" element={<RegionDepartment/>}/>
                    <Route path="/department" element={<Department/>}/>
                    <Route path="/multiMenu" element={<MultiMenu/>}/>
                    <Route path="/multi-menu-one/:id" element={<MultiMenuOne/>}/>
                    <Route path="/relation-view" element={<MenuView/>}/>
                    <Route path="/relation-menu" element={<RelationMenu/>}/>
                    <Route path="/supplier" element={<Supplier/>}/>
                    <Route path="/price" element={<Price/>}/>
                    <Route path="/contract" element={<Contract/>}/>
                    <Route path="/create-contract" element={<CreateContract/>}/>
                    <Route path="/edit-contract/:id" element={<EditContract/>}/>
                    <Route path="/children-number" element={<ChildrenNumber/>}/>
                    <Route path="/warehouse" element={<Warehouse/>}/>
                    <Route path="/warehouser" element={<Warehouser/>}/>
                    <Route path="/warehouse-admin" element={<Warehouser/>}/>
                    <Route path="/report-warehouse" element={<InputOutput/>}/>
                    <Route path="/report" element={<Report/>}/>
                    <Route path="/one-day-menu/:id" element={<GetOneDayMenu/>}/>
                    <Route path="/one-day-menu" element={<OneDayMenu/>}/>
                    <Route path="/menu-mtt" element={<OneDayWithMttFromUsers/>}/>
                    <Route path="/input-output-admin" element={<InputOutputFromAdmin/>}/>
                    <Route path="/report-paramedic" element={<InputOutputKidsNumber/>}/>
                    <Route path="/report-kids-number" element={<InputOutputKidsNumberFromAdmin/>}/>
                    <Route path="/notification" element={<Notification/>}/>
                    <Route path="/send-notification" element={<SendNotifications/>}/>
                    <Route path="/product-pack" element={<ProductPack/>}/>
                    <Route path="/permission" element={<PermissionsFromRelation/>}/>
                    <Route path="/default-kids-number" element={<KidsNumberOther/>}/>
                    <Route path="/one-multi-menu-other/:id" element={<MultiMenuOneFromOther/>}/>
                    <Route path="/order/:id" element={<OneOrder/>}/>
                    <Route path="/order" element={<Order/>}/>
                    <Route path="/waste" element={<Waste/>}/>
                </Routes>
                <br/><br/>
            </Sidebar>
        </div>
    );
}

export default Sidebars;
