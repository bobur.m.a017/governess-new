import React, {useEffect, useMemo, useRef, useState} from 'react';
import {Form} from 'react-bootstrap';
import {useDispatch, useSelector} from "react-redux";
import {toast} from 'react-toastify';
import {getAddress} from './AddressReducer';
import districtStyle from "../more/DistrictStyle";

function Address({district, region, view}) {
    const [districtState, setDistrictState] = useState();
    const [regionList, setRegionList] = useState([]);
    const [regionState, setRegionState] = useState();
    const [addressList, setAddressList] = useState([]);
    const address = useSelector(state => state.address.address)
    const error = useSelector(state => state.address.error)
    const dispatch = useDispatch();
    const firstUpdate = useRef(false);


    useEffect(() => {
        setAddressList(address);
    }, [address]);


    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            if (address.length === 0) {
                dispatch(getAddress());
            }
        }
    }, []);


    const getRegion = (e) => {
        var list = addressList.filter(item => item.id === parseInt(e.target.value));
        if (list[0] !== undefined) {
            setRegionList(list[0].districtList);
            if (region) {
                region(list[0])
            }
            setRegionState(list[0]);
        }
    }
    const getDistrict = (e) => {
        var list = regionList.filter(item => item.id === parseInt(e.target.value));
        if (list[0] !== undefined) {
            district(list[0]);
            setDistrictState(list[0])
        }
    }

    return (
        <>
            <Form.Select required name='region' onChange={getRegion} value={regionState?.id || ""}>
                <option value="">Viloyatni tanlang</option>
                {
                    addressList?.map((item, index) => (
                        <option key={index} value={item.id}>{item.name}</option>
                    ))
                }
            </Form.Select>
            <br/>
            {view ? <Form.Select required name='district' value={districtState?.id || ""} onChange={getDistrict}>
                <option value="">Tumanni tanlang</option>
                {
                    regionList?.map((item, index) => (
                        <option key={index} value={item.id}>{item.name}</option>
                    ))
                }
            </Form.Select> : null}
        </>
    );
}

export default Address;
