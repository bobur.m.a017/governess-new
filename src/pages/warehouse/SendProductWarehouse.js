import React from 'react';
import {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Col, Container, Form, Modal, Row} from "react-bootstrap";
import {getAcceptedProduct, getAcceptedProductAll, getWarehouse} from "./WarehouseReducer";
import NavbarHeader from "../more/NavbarHeader";
import FromPageSizeBottom from "../fromPage/FromPageSizeBottom";
import {TimestampToInputDate} from "../funcs/Funcs";
function Warehouse() {
    const [wareHouseState, setWareHouseState] = useState();
    const [productReceived, setProductReceived] = useState({
        "id": "",
        "receivedWeight": 0
    });
    const [currentNavs, setCurrentNavs] = useState(0);
    const [currentNumber, setCurrentNumber] = useState(0);
    const [inOutList, setInOutList] = useState([]);
    const [productState, setProductState] = useState();
    const [productCurrent, setProductCurrent] = useState();
    const warehouses = useSelector(state => state.warehouse.warehouses);
    const result = useSelector(state => state.warehouse.result);
    const acceptedProduct = useSelector(state => state.warehouse.acceptedProduct);
    const acceptedProducts = useSelector(state => state.warehouse.acceptedProducts);
    const getFiless = useSelector(state => state.getFiles.getFiless);
    const dispatch = useDispatch();
    const firstUpdate = useRef(false);
    const [show2, setShow2] = useState(false);
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleClose2 = () => setShow2(false);
    const handleShow = (num) => {
        setCurrentNumber(num);
        setShow(true);
    }
    const handleShow2 = (list) => {
        setInOutList(list);
        setShow2(true);
    }

    useEffect(() => {
        if (!firstUpdate.current) {

        } else {

        }
    }, [getFiless]);

    useEffect(() => {
        if (!firstUpdate.current) {

        } else {
            dispatch(getAcceptedProduct());
            dispatch(getAcceptedProductAll());
            dispatch(getWarehouse());
            handleClose();
        }
    }, [result]);

    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            // dispatch(getWarehouse());
        } else {
            setWareHouseState(warehouses);
        }
    }, [acceptedProduct]);

    const onClickProduct = (data) => {
        setProductReceived({...data, packWeight: '', date: Date.now()});
        handleShow(1);
    }

    const submit = (e) => {
        e.preventDefault();
    }

    return (
        <div className={"allMain"}>
            <NavbarHeader
                navs={[{name: "Ombordagi mahsulotlar"}, {name: "Qabul qilingan mahsulotlar"}]}
                currentNavs={setCurrentNavs}/>
            <Container fluid={true} className={'mt-3'}>
                {currentNavs === 0 ? <Row>
                    {warehouses?.list?.length > 0 ? <Col className={'figma-card'}>
                        <div className={'w-100 d-flex justify-content-end'}>
                            <button className={'buttonPdf my-2'} >PDF</button>
                        </div>

                    </Col> : warehouses?.list ?
                        <div className={"text-center fs-3"} style={{color: 'red'}}>Ma'lumot mavjud emas </div> :
                        <div className={"text-center fs-3"} style={{color: 'red'}}>Qabul qilingan mahsulotlar mavjud
                            emas</div>}
                </Row> : null}
                {currentNavs === 1 ? <Row>
                    <Col className={'figma-card'}>
                        {acceptedProducts?.list?.length > 0 ? <div className={'tableCalendar'}>
                            <table>
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Mahsulot nomi</th>
                                    <th>Narxi</th>
                                    <th>Shartnoma</th>
                                    <th>Shartnomachi</th>
                                    <th>Vazni</th>
                                    <th>Qadoqlar soni</th>
                                    <th>Qadoq miqdori</th>
                                    <th>Vaqti</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    acceptedProducts?.list?.map((product, index) =>
                                        <tr key={index} style={{cursor: 'pointer'}}>
                                            <td>{index + 1}</td>
                                            <td>{product?.producyName}</td>
                                            <td>{product?.price}</td>
                                            <td>{product?.shartnomaRaqami}</td>
                                            <td>{product?.yetkazibBeruvchi}</td>
                                            <td>{product?.weight}</td>
                                            <td>{product?.packWeight}</td>
                                            <td>{product?.pack}</td>
                                            <td>{TimestampToInputDate(product?.date)}</td>
                                        </tr>
                                    )
                                }
                                </tbody>
                            </table>
                            <br/>
                            <FromPageSizeBottom currentPage={acceptedProducts.getPageNumber}
                                                pageSize={acceptedProducts?.getPageSize}
                                                allPageSize={acceptedProducts?.allPageSize}/>
                        </div> : warehouses?.list ?
                            <div className={"text-center fs-3"} style={{color: 'red'}}>Ma'lumot mavjud emas </div> :
                            <div className={"text-center fs-3"} style={{color: 'red'}}>Omborda ma'lumot mavjud
                                emas</div>}
                    </Col>
                </Row> : null}
            </Container>
            <Modal show={show2} onHide={handleClose2} size={"xl"}>
                <Modal.Header closeButton>
                </Modal.Header>
                <Modal.Body>
                    <div className={'tableCalendar'}>
                        <table>
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Narxi</th>
                                <th>Umumiy miqdori</th>
                                <th>Qadoqlar soni</th>
                                <th>Qadoq miqdori</th>
                                <th>Qabul qilingan sana</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                inOutList?.map((product, index) =>
                                    <tr key={index} style={{cursor: 'pointer'}}>
                                        <td>{index + 1}</td>
                                        <td>{product.price}</td>
                                        <td>{product.weight}</td>
                                        <td>{product.packWeight}</td>
                                        <td>{product.pack}</td>
                                        <td>{TimestampToInputDate(product?.createDate)}</td>
                                    </tr>
                                )
                            }
                            </tbody>
                        </table>
                        <br/>
                        {/*<FromPageSizeBottom currentPage={acceptedProduct.getPageNumber}*/}
                        {/*                    pageSize={acceptedProduct?.getPageSize} changesPage={changePage2}*/}
                        {/*                    allPageSize={acceptedProduct?.allPageSize}/>*/}
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Bekor qilish
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default Warehouse;