import React from 'react';
import {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Col, Container, Form, InputGroup, Modal, Row} from "react-bootstrap";
import {
    addProductContract, addProductSpend, addProductWareHouse,
    addReceivedProduct,
    getAcceptedProduct, getAcceptedProduct2,
    getAcceptedProductAll, getKindergartenWarehouse,
    getWarehouse
} from "./WarehouseReducer";
import NavbarHeader from "../more/NavbarHeader";
import {getFilesMttWarehouse, getGetFiles} from "../getFiles/GetFilesReducer";
import {getRoleStorage} from "../more/Functions";
import FromPageSizeBottom from "../fromPage/FromPageSizeBottom";
import {TimestampToInputDate} from "../funcs/Funcs";
import {addWasteWarehouse} from "../waste/WasteReducer";
import {getProduct} from "../product/ProductReducer";
import DropdownCustom from "../more/DropdownCustom";

function Warehouse() {
    const addProd = {
        id:'',
        productId:'',
        weight:'',
        productName:"Mahsulotni tanlasg"
    }
    const [wareHouseState, setWareHouseState] = useState();
    const [productReceived, setProductReceived] = useState({
        "id": "",
        "receivedWeight": 0
    });
    const [currentNavs, setCurrentNavs] = useState(0);
    const [currentNumber, setCurrentNumber] = useState(0);
    const [productSpend, setProductSpend] = useState([]);
    const [accProduct, setAccProduct] = useState([]);
    const [addProduct, setAddProduct] = useState(addProd);
    const warehouses = useSelector(state => state.warehouse.warehouses);
    const result = useSelector(state => state.warehouse.result);
    const result2 = useSelector(state => state.waste.result);
    const kindergartenWarehouse = useSelector(state => state.warehouse.kindergartenWarehouse);
    const acceptedProduct = useSelector(state => state.warehouse.acceptedProduct);
    const acceptedProducts = useSelector(state => state.warehouse.acceptedProducts);
    const {products,resultProduct} = useSelector(state => state.product);
    const getFiless = useSelector(state => state.getFiles.getFiless);
    const dispatch = useDispatch();
    const firstUpdate = useRef(false);
    const [show2, setShow2] = useState(false);
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleClose2 = () => setShow2(false);
    const handleShow = (num) => {
        setCurrentNumber(num);
        setAddProduct(addProd);
        setShow(true);
    }
    const handleShow2 = (data, num) => {
        setProductSpend(data);
        setCurrentNumber(num);
        setShow(true);
    }

    useEffect(() => {
        if (firstUpdate.current) {
        }
    }, [kindergartenWarehouse]);

    useEffect(() => {
        if (!firstUpdate.current) {

        } else {
            dispatch(getAcceptedProduct());
            dispatch(getKindergartenWarehouse());
            dispatch(getAcceptedProduct2());
            handleClose();
        }
    }, [result, result2,resultProduct]);

    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            dispatch(getProduct());
            dispatch(getKindergartenWarehouse());
            dispatch(getAcceptedProduct());
            dispatch(getAcceptedProduct2());
        } else {
            let pro = {...productReceived};
            setProductReceived({});
            setWareHouseState(warehouses);
            setAccProduct(acceptedProduct);
            setTimeout(() => {
                setProductReceived(pro);
            }, 80);
        }
    }, [acceptedProduct]);

    const onClickProduct = (data) => {
        if (data.id === productReceived.id) {
            setProductReceived({});
        } else {
            setProductReceived(data);
        }
        // handleShow(1);
    }

    const submit = (e) => {
        e.preventDefault();
        dispatch(addProductContract(productReceived));
    }

    const onChangeProductWeight = (e) => {
        if (e.target.name === "date") {
            setProductReceived({...productReceived, [e.target.name]: new Date(e.target.value).getTime()});
        } else {
            let packWeight = productReceived?.pack > 0 ? parseInt(e.target.value) : e.target.value;
            let receivedWeight = productReceived?.pack > 0 ? (parseInt(e.target.value) * productReceived?.pack) : e.target.value;
            setProductReceived({...productReceived, receivedWeight, packWeight});
        }
    }

    const getPdf = (e) => {
        dispatch(getFilesMttWarehouse());
    }
    const changePage1 = (page) => {
        dispatch(getAcceptedProductAll({page, pageSize: 20}));
    }
    const changePage0 = (pageNumber) => {
        dispatch(getKindergartenWarehouse({pageNumber, pageSize: 20}));
    }
    const changePage2 = (page) => {
        dispatch(getAcceptedProduct({page, pageSize: 20}));
    }
    const inputProduct = () => {
        return (
            <Form onSubmit={submit}>
                <Modal.Header closeButton>
                    <Modal.Title>{productReceived?.productName}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                        <span className={'mb-3'} style={{color: '#fcb713'}}>Maximal kiritish miqdori:
                            <span style={{color: '#000'}}>{productReceived?.residualWeight}</span></span>
                    <br/>
                    <Form.Label>Sana</Form.Label>
                    <Form.Control type={'date'} name={"date"}
                                  value={TimestampToInputDate(productReceived.date)}
                                  onChange={onChangeProductWeight}
                                  onWheel={event => event.target.blur()}
                                  max={TimestampToInputDate(productReceived.date)}/>
                    <Form.Label>Qabul qilinadigan miqdor</Form.Label>
                    <Form.Control max={productReceived?.residualPackWeight} type={'number'} name={"packWeight"}
                                  value={productReceived.packWeight} step={'0.001'}
                                  onChange={onChangeProductWeight}
                                  onWheel={event => event.target.blur()} required/>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Bekor qilish
                    </Button>
                    <Button variant="primary" type={'submit'}>
                        Tayyor
                    </Button>
                </Modal.Footer>
            </Form>
        )
    }
    const submitWaste = (e) => {
        e.preventDefault();
        dispatch(addWasteWarehouse(productSpend?.productId, {weight: e.target.waste.value}));
    }
    const wasteProduct = () => {
        return (
            <Form onSubmit={submitWaste}>
                <Modal.Header>{productSpend?.productName} miqdori:{productSpend?.packWeight}</Modal.Header>
                <Modal.Body>
                    <Form.Label>KG / DONA</Form.Label>
                    <Form.Control type={"number"} name={"waste"} step={"0.01"} onWheel={e => e.target.blur()}
                                  min={0}
                                  max={productSpend?.packWeight}/>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant={"danger"} onClick={handleClose}>Ortga</Button>
                    <Button variant={"primary"} type={"submit"}>Tayyor</Button>
                </Modal.Footer>
            </Form>
        )
    }
    const submitProductReceived = (data, delivery) => (e) => {
        e.preventDefault();
        dispatch(addReceivedProduct({
            ...data,
            packWeight: e.target.packWeight.value,
            kindergartenId: delivery?.kindergartenId
        }));
    }
    const submitProduct = (e) => {
        e.preventDefault();
        dispatch(addProductSpend(productSpend?.productId, {weight: e.target.weight.value}));
    }
    const submitAddProduct = (e) => {
        e.preventDefault();
        console.log(addProduct,"addProd")
        dispatch(addProductWareHouse(addProduct?.productId, {weight: e.target.weight.value}));
    }

    const renderProductSpend = () => {
        return (
            <Form onSubmit={submitProduct}>
                <Modal.Header closeButton>{productSpend?.productName}
                </Modal.Header>
                <Modal.Body>
                    <InputGroup size="sm" className="mb-3">
                        <InputGroup.Text> <span
                            className={"fw-bolder mx-2"}>Max miqdor {productSpend?.packWeight}</span>
                            KG / DONA
                        </InputGroup.Text>
                        <Form.Control
                            type={"number"}
                            onWheel={e => e.target.blur()}
                            step={"0.01"}
                            required={true}
                            name={"weight"}
                            max={productSpend?.packWeight}
                            min={0}
                        />
                    </InputGroup>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="primary" type={"submit"}>
                        Tayyor
                    </Button>
                    <Button variant="secondary" onClick={handleClose}>
                        Bekor qilish
                    </Button>
                </Modal.Footer>
            </Form>
        )
    }
    const renderWaste = () => {
        return (
            <Form onSubmit={submitWaste}>
                <Modal.Header closeButton>
                </Modal.Header>
                <Modal.Body>
                    <InputGroup size="sm" className="mb-3">
                        <InputGroup.Text>Mahsulot vazni</InputGroup.Text>
                        <Form.Control
                            type={"number"}
                            onWheel={e => e.target.blur()}
                            step={"0.01"}
                            required={true}
                            name={"weight"}
                            min={0}
                        />
                    </InputGroup>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Bekor qilish
                    </Button>
                </Modal.Footer>
            </Form>
        )
    }
    const addProductRender = () => {
        return (
            <Form onSubmit={submitAddProduct}>
            <Modal.Header closeButton>
                <DropdownCustom name={addProduct?.productName} list={products} setData={getProductOne} itemName={"name"}/>
            </Modal.Header>
            <Modal.Body>
                <InputGroup size="sm" className="mb-3">
                    <InputGroup.Text>
                        KG / DONA
                    </InputGroup.Text>
                    <Form.Control
                        type={"number"}
                        onWheel={e => e.target.blur()}
                        step={"0.01"}
                        required={true}
                        name={"weight"}
                        max={addProd?.weight}
                        min={0}
                    />
                </InputGroup>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary" type={"submit"}>
                    Tayyor
                </Button>
                <Button variant="secondary" onClick={handleClose}>
                    Bekor qilish
                </Button>
            </Modal.Footer>
        </Form>)
    }
    const getProductOne = (data) => {
        console.log(data)
      setAddProduct({...addProduct,productId: data?.id,productName:data?.name});
    }
    return (
        <div className={"allMain"}>
            <NavbarHeader
                navs={[{name: "Ombordagi mahsulotlar"}, {name: "Qabul qilingan mahsulotlar"}, getRoleStorage() === "ROLE_OMBORCHI" ? {name: "Mahsulot qabul qilish"} : '']}
                currentNavs={setCurrentNavs}/>
            <Container fluid={true} className={'mt-3'}>
                {currentNavs === 0 ? <Row>
                    {kindergartenWarehouse?.list?.length > 0 ? <Col className={'figma-card'}>
                        <div className={'w-100 d-flex justify-content-end'}>
                            <div>
                            <button className={'buttonPdf my-2'} onClick={getPdf}>PDF</button>
                            <button className={"createButtons mb-3"} onClick={() => handleShow(5)}>
                                Mahsulot kiritish
                            </button>
                            </div>
                        </div>
                        <div className={'tableCalendar'}>
                            <table>
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Mahsulot nomi</th>
                                    <th>Miqdor</th>
                                    <th>Qadoqlar soni</th>
                                    <th>Chiqid</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    kindergartenWarehouse?.list?.map((product, index) =>
                                        <tr key={index}
                                        >
                                            <td>{index + 1}</td>
                                            <td style={{cursor: 'pointer'}}
                                                onClick={() => handleShow2(product, 3)}>{product.productName}</td>
                                            <td style={{cursor: 'pointer'}}
                                                onClick={() => handleShow2(product, 3)}>{product.weight}</td>
                                            <td style={{cursor: 'pointer'}}
                                                onClick={() => handleShow2(product, 3)}>{product.packWeight}</td>
                                            <td>
                                                <button className={"buttonPdf"}
                                                        onClick={() => handleShow2(product, 2)}>Yaroqsizga
                                                </button>
                                            </td>
                                        </tr>
                                    )
                                }
                                </tbody>
                            </table>
                            <br/>
                            <FromPageSizeBottom currentPage={kindergartenWarehouse.getPageNumber}
                                                pageSize={kindergartenWarehouse?.getPageSize} changesPage={changePage0}
                                                allPageSize={kindergartenWarehouse?.allPageSize}/>
                        </div>
                    </Col> : kindergartenWarehouse?.list ?
                        <div className={"text-center fs-3"} style={{color: 'red'}}>
                            <div>
                                <button className={"createButtons mb-3"} onClick={() => handleShow(5)}>
                                Mahsulot kiritish
                            </button>
                            </div>
                            <div className={"figma-card"}>

                                Qabul qilingan mahsulotlar mavjud
                                emas
                            </div>
                        </div> :
                        <div className={"text-center fs-3"} style={{color: 'red'}}>
                            <div>
                                <button className={"createButtons mb-3"} onClick={() => handleShow(5)}>
                                    Mahsulot kiritish
                                </button>
                            </div>
                            <div className={"figma-card"}>
                                Qabul qilingan mahsulotlar mavjud
                                emas
                            </div>
                        </div>
                       }
                </Row> : null}
                {currentNavs === 2 ? <Row>
                    <Col className={'figma-card'}>
                        {accProduct?.length > 0 ? <div className={'tableCalendar'}>
                            <table>
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th style={{minWidth: 100}}>Sana</th>
                                    <th>Yaetqazuvchi</th>
                                    <th>Holati</th>
                                </tr>
                                </thead>
                                {
                                    accProduct?.map((product, index) =>
                                        <tbody key={index}>
                                        <tr style={{cursor: 'pointer'}}
                                            onClick={() => onClickProduct(product)}>
                                            <td>{index + 1}</td>
                                            <td>{TimestampToInputDate(product?.date)}</td>
                                            <td>{product?.deliver}</td>
                                            <td>{product?.status}</td>
                                        </tr>
                                        {productReceived?.id === product?.id ? <tr className={"animate"}>
                                            <td colSpan={4}>
                                                <div className={"miniTable2"} style={{borderColor: 'red'}}>
                                                    <table style={{backgroundColor: 'white'}}>
                                                        <thead>
                                                        <tr>
                                                            <th>№</th>
                                                            <th>Mahsulot nomi</th>
                                                            <th>Miqdori / Dona</th>
                                                            <th>Qabul qilish</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        {
                                                            product?.deliverySubDTOList?.map((item, index2) =>
                                                                <tr key={index2}>
                                                                    <td>{index2 + 1}</td>
                                                                    <td>{item.productName}</td>
                                                                    <td>{item.packWeight}</td>
                                                                    <td className={"d-flex justify-content-center align-items-center"}>
                                                                        {item.status === "YANGI" ? <Form
                                                                                id={item.id}
                                                                                onSubmit={submitProductReceived(item, product)}>
                                                                                <InputGroup className="mb-3">
                                                                                    <Form.Control
                                                                                        type={"number"}
                                                                                        name={"packWeight"}
                                                                                        step={"0.001"}
                                                                                        onWheel={e => e.target.blur()}
                                                                                        max={item.packWeight}
                                                                                        min={0}
                                                                                        style={{maxWidth: 110}}
                                                                                        defaultValue={item.packWeight}
                                                                                        required
                                                                                    />
                                                                                    <InputGroup.Text id="basic-addon1">
                                                                                        <button className={"buttonExcel"}
                                                                                                type={"submit"}
                                                                                                form={item.id}>Qabul qilish
                                                                                        </button>
                                                                                    </InputGroup.Text>
                                                                                </InputGroup>
                                                                            </Form> :
                                                                            <span>{item?.status}:{item?.kinWeight}</span>}
                                                                    </td>
                                                                </tr>
                                                            )
                                                        }
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr> : null}
                                        </tbody>
                                    )
                                }
                            </table>
                            <br/>
                            <FromPageSizeBottom currentPage={accProduct.getPageNumber}
                                                pageSize={accProduct?.getPageSize} changesPage={changePage2}
                                                allPageSize={accProduct?.allPageSize}/>
                        </div> : accProduct?.list ?
                            <div className={"text-center fs-3"} style={{color: 'red'}}>Ma'lumot mavjud emas </div> :
                            <div className={"text-center fs-3"} style={{color: 'red'}}>Qabul qilingan mahsulotlar mavjud
                                emas</div>}
                    </Col>
                </Row> : null}
                {currentNavs === 1 ? <Row>
                    <Col className={'figma-card'}>
                        {acceptedProducts?.list?.length > 0 ? <div className={'tableCalendar'}>
                            <table>
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Vaqti</th>
                                    <th>Taminotchi</th>
                                    <th>Holati</th>
                                </tr>
                                </thead>
                                {
                                    acceptedProducts?.list?.map((product, index) =>
                                        <tbody key={index}>
                                        <tr style={{cursor: 'pointer'}}
                                            onClick={() => onClickProduct(product)}>
                                            <td>{index + 1}</td>
                                            <td>{TimestampToInputDate(product?.date)}</td>
                                            <td>{product?.deliver}</td>
                                            <td>{product?.status}</td>
                                        </tr>
                                        {productReceived?.id === product?.id ? <tr className={"animate"}>
                                            <td colSpan={4}>
                                                <div className={"miniTable2"} style={{borderColor: 'red'}}>
                                                    <table style={{backgroundColor: 'white'}}>
                                                        <thead>
                                                        <tr>
                                                            <th>№</th>
                                                            <th>Mahsulot nomi</th>
                                                            <th>Qabul qilingan miqdor</th>
                                                            <th>Qoldiq miqdor</th>
                                                            <th>Yuborilgan miqdor</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        {
                                                            product?.deliverySubDTOList?.map((item, index2) =>
                                                                <tr key={index2}>
                                                                    <td>{index2 + 1}</td>
                                                                    <td>{item.productName}</td>
                                                                    <td>{item?.kinPackWeight}</td>
                                                                    <td>{item?.restPackWeight}</td>
                                                                    <td>{item.packWeight}</td>
                                                                </tr>
                                                            )
                                                        }
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr> : null}
                                        </tbody>
                                    )
                                }
                            </table>
                            <br/>
                            <FromPageSizeBottom currentPage={acceptedProducts.getPageNumber}
                                                pageSize={acceptedProducts?.getPageSize} changesPage={changePage1}
                                                allPageSize={acceptedProducts?.allPageSize}/>
                        </div> : acceptedProducts?.list ?
                            <div className={"text-center fs-3"} style={{color: 'red'}}>Ma'lumot mavjud emas </div> :
                            <div className={"text-center fs-3"} style={{color: 'red'}}>Omborda ma'lumot mavjud
                                emas</div>}
                    </Col>
                </Row> : null}
            </Container>
            <Modal show={show} onHide={handleClose}>
                {currentNumber === 1 ? inputProduct() : null}
                {currentNumber === 2 ? wasteProduct() : null}
                {currentNumber === 3 ? renderProductSpend() : null}
                {currentNumber === 4 ? renderWaste() : null}
                {currentNumber === 5 ? addProductRender() : null}
            </Modal>
            <Modal show={show2} onHide={handleClose2} size={"xl"}>

            </Modal>
        </div>
    );
}

export default Warehouse;
