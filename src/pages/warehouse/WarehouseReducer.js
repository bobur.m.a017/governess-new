import {createSlice} from "@reduxjs/toolkit";
import {toast} from "react-toastify";
import {apiCall} from "../../ApiCall";
import {getToken, toastError} from "../more/Functions";

const slice = createSlice({
    name: "warehouse",
    initialState: {
        result: {},
        error: {},
        warehouses: [],
        acceptedProduct: [],
        acceptedProducts: [],
        sendProductByKinder: [],
        sendWarehouseIonut: {},
        kindergartenWarehouse: [],
        delivery: {},
    },
    reducers: {
        warehouses: (state, action) => {
            state.warehouses = action.payload;
        },
        sendWarehouseIonut: (state, action) => {
            state.sendWarehouseIonut = action.payload;
        },
        sendWarehousePdf: (state, action) => {
            var win = window.open('http://' + action.payload, '_blank');
            win.focus();
        },
        delivery: (state, action) => {
            state.delivery = action.payload;
        },
        kindergartenWarehouse: (state, action) => {
            state.kindergartenWarehouse = action.payload;
        },
        sendProductByKinder: (state, action) => {
            state.sendProductByKinder = action.payload;
        },
        acceptedProduct: (state, action) => {
            state.acceptedProduct = action.payload;
        },
        acceptedProducts: (state, action) => {
            state.acceptedProducts = action.payload;
        },
        resultReducer: (state, action) => {
            state.result = action.payload;
            toast.success(action.payload?.text);
        },
        errorReducer: (state, action) => {
            state.error = action.payload;
            toastError(action.payload)
        },
    }
})

export const getWarehouse = (params) => apiCall({
    url: "/warehouse",
    method: "GET",
    headers: {
        Authorization: getToken(),
    },
    params,
    success: slice.actions.warehouses.type,
    error: slice.actions.errorReducer.type
})

export const getWarehouseInout = (params) => apiCall({
    url: "/acceptedProduct/byDate",
    method: "GET",
    headers: {
        Authorization: getToken(),
    },
    params,
    success: slice.actions.sendWarehouseIonut.type,
    error: slice.actions.errorReducer.type
})

export const getWarehousePdf = (params) => apiCall({
    url: "/acceptedProduct/pdf/byDate",
    method: "GET",
    headers: {
        Authorization: getToken(),
    },
    params,
    success: slice.actions.sendWarehousePdf.type,
    error: slice.actions.errorReducer.type
})

export const getAcceptedProduct = (params) => apiCall({
    url: "/delivery/statusNew",
    method: "GET",
    headers: {
        Authorization: getToken(),
    },
    params,
    success: slice.actions.acceptedProduct.type,
    error: slice.actions.errorReducer.type
})

export const getAcceptedProduct2 = (params) => apiCall({
    url: "/delivery/statusNotNew",
    method: "GET",
    headers: {
        Authorization: getToken(),
    },
    params,
    success: slice.actions.acceptedProducts.type,
    error: slice.actions.errorReducer.type
})

export const getAllWarehouse = (params) => apiCall({
    url: "/delivery",
    method: "GET",
    headers: {
        Authorization: getToken(),
    },
    params,
    success: slice.actions.delivery.type,
    error: slice.actions.errorReducer.type
})

export const getCargoLetter = (id) => apiCall({
    url: "/delivery/getFile/"+id,
    method: "GET",
    headers: {
        Authorization: getToken(),
    },
    success: slice.actions.sendWarehousePdf.type,
    error: slice.actions.errorReducer.type
})

export const getWareHoseExcel = (params) => apiCall({
    url: "/warehouse/getFileEcxel",
    method: "GET",
    headers: {
        Authorization: getToken(),
    },
    params,
    success: slice.actions.sendWarehousePdf.type,
    error: slice.actions.errorReducer.type
})


export const addWarehouse = (params,id) => apiCall({
    url: "/warehouse/"+id,
    method: "POST",
    headers: {
        Authorization: getToken(),
    },
    params,
    success: slice.actions.resultReducer.type,
    error: slice.actions.errorReducer.type
})

export const addProductContract = (data) => apiCall({
    url: "/acceptedProduct/"+data.id,
    method: "POST",
    headers: {
        Authorization: getToken(),
    },
    data,
    success: slice.actions.resultReducer.type,
    error: slice.actions.errorReducer.type
})

export const addDelivery = (data) => apiCall({
    url: "/delivery",
    method: "POST",
    headers: {
        Authorization: getToken(),
    },
    data,
    success: slice.actions.resultReducer.type,
    error: slice.actions.errorReducer.type
})

export const getAcceptedProductAll = () => apiCall({
    url: "/acceptedProduct",
    method: "GET",
    headers: {
        Authorization: getToken(),
    },
    success: slice.actions.acceptedProducts.type,
    error: slice.actions.errorReducer.type
})

export const getAcceptedProductAll2 = () => apiCall({
    url: "/acceptedProduct",
    method: "GET",
    headers: {
        Authorization: getToken(),
    },
    success: slice.actions.acceptedProducts.type,
    error: slice.actions.errorReducer.type
})

export const getSendProductByKinder = (id) => apiCall({
    url: "/order/getProductByKindergarten/"+id,
    method: "GET",
    headers: {
        Authorization: getToken(),
    },
    success: slice.actions.sendProductByKinder.type,
    error: slice.actions.errorReducer.type
})

export const editWarehouse = (data) => apiCall({
    url: "/warehouse/" + data.id,
    method: "PUT",
    headers: {
        Authorization: getToken(),
    },
    data,
    success: slice.actions.resultReducer.type,
    error: slice.actions.errorReducer.type
})


export const addReceivedProductWar = (data,id) => apiCall({
    url: "/warehouse/accept/" + id,
    method: "POST",
    headers: {
        Authorization: getToken(),
    },
    data,
    success: slice.actions.resultReducer.type,
    error: slice.actions.errorReducer.type
})

export const addReceivedProduct = (data) => apiCall({
    url: "/kindergartenWarehouse",
    method: "POST",
    headers: {
        Authorization: getToken(),
    },
    data,
    success: slice.actions.resultReducer.type,
    error: slice.actions.errorReducer.type
})

export const addProductSpend = (id,params) => apiCall({
    url: "/kindergartenWarehouse/"+id,
    method: "POST",
    headers: {
        Authorization: getToken(),
    },
    params,
    success: slice.actions.resultReducer.type,
    error: slice.actions.errorReducer.type
})

export const addProductWareHouse = (id,params) => apiCall({
    url: "/kindergartenWarehouse/add/"+id,
    method: "POST",
    headers: {
        Authorization: getToken(),
    },
    params,
    success: slice.actions.resultReducer.type,
    error: slice.actions.errorReducer.type
})

export const getKindergartenWarehouse = (params) => apiCall({
    url: "/kindergartenWarehouse",
    method: "GET",
    headers: {
        Authorization: getToken(),
    },
    params,
    success: slice.actions.kindergartenWarehouse.type,
    error: slice.actions.errorReducer.type
})


export default slice.reducer;
