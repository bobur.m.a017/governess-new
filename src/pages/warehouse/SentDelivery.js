import React from 'react';
import {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Col, Container, Form, InputGroup, Row} from "react-bootstrap";
import {TimestampToInputDate} from "../funcs/Funcs";
import FromPageSizeBottom from "../fromPage/FromPageSizeBottom";
import {
    addReceivedProduct,
    addReceivedProductWar,
    getAcceptedProduct,
    getAcceptedProduct2,
    getAllWarehouse, getCargoLetter, getWarehouse
} from "./WarehouseReducer";

function SentDelivery(props) {
    const [state, setState] = useState();
    const [productReceived, setProductReceived] = useState({});
    const [params, setParams] = useState({
        startDate: '',
        endDate: '',
        pageNumber: 0,
        pageSize: 20,
        status: '',
        MTTNumber: ''
    });
    const dispatch = useDispatch();
    const delivery = useSelector(state => state.warehouse.delivery);
    const result = useSelector(state => state.warehouse.result);
    const firstUpdate = useRef(false);
    const changePage1 = () => {

    }
    useEffect(() => {
        if (firstUpdate.current) {
            let data = {...productReceived};
            setProductReceived({});
            setTimeout(() => {
                setProductReceived(data);
            }, 80);
        }
    }, [delivery]);

    useEffect(() => {
        if (firstUpdate.current) {
            let data = {...productReceived};
            setProductReceived({});
            dispatch(getAllWarehouse(params));
            dispatch(getWarehouse({pageNumber: 0, pageSize: 20}));
        }
    }, [result]);

    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
        }
    }, []);
    const submitFilter = (e) => {
        e.preventDefault();
        dispatch(getAllWarehouse(params));
    }
    const changeParams = (e) => {
        if (e.target.type === "date") {
            setParams({...params, [e.target.name]: new Date(e.target.value).getTime()})
        } else {
            setParams({...params, [e.target.name]: e.target.value})
        }
    }
    const changePage2 = (pageNumber) => {
        dispatch(getAllWarehouse({...params, pageNumber}));
    }

    const onClickProduct = (data) => {
        if (data.id === productReceived.id) {
            setProductReceived({});
        } else {
            setProductReceived(data);
        }
        // handleShow(1);
    }
    const submitProductReceived = (data, deliveryThis) => (e) => {
        e.preventDefault();
        let pack;
        let packWeight;
        let weight;
        if (data?.pack > 0) {
            pack = data?.pack;
            packWeight = parseFloat(e.target.packWeight.value).toFixed(2);
            weight = (pack * parseFloat(e.target.packWeight.value).toFixed(2));
        } else {
            pack = data?.pack;
            packWeight = parseFloat(e.target.packWeight.value).toFixed(2);
            weight = parseFloat(e.target.packWeight.value).toFixed(2);
        }
        dispatch(addReceivedProductWar({
            ...data,
            pack,
            packWeight,
            weight,
            kindergartenId: deliveryThis?.kindergartenId
        }, deliveryThis?.id));
    }

    return (
        <div>
            <Container fluid={true}>
                <Form onSubmit={submitFilter}>
                    <Row>
                        <Col md={3}>
                            <Form.Label>MTT raqami</Form.Label>
                            <Form.Control size={'sm'} type={"number"} value={params?.MTTNumber}
                                          name={"MTTNumber"} onChange={changeParams}/>
                        </Col>
                        <Col md={3}>
                            <Form.Label>Toifasi</Form.Label>
                            <Form.Select size={'sm'} value={params.status} onChange={changeParams} name={"status"}
                                         required>
                                <option value={""}>Tanlang</option>
                                <option value={"YUBORILDI"}>YUBORILDI</option>
                                <option value={"QISMAN QABUL QILINDI"}>QISMAN QABUL QILINDI</option>
                                <option value={"TO`LIQ YETKAZIB BERILDI"}>TO`LIQ YETKAZIB BERILDI</option>
                            </Form.Select>
                        </Col>
                        <Col md={2}>
                            <Form.Label>Boshlanish sana</Form.Label>
                            <Form.Control size={'sm'} type={"date"} value={TimestampToInputDate(params.startDate)}
                                          name={"startDate"} onChange={changeParams} required/>
                        </Col>
                        <Col md={2}>
                            <Form.Label>Boshlanish sana</Form.Label>
                            <Form.Control size={'sm'} type={"date"} value={TimestampToInputDate(params.endDate)}
                                          name={"endDate"} min={TimestampToInputDate(params.startDate)}
                                          onChange={changeParams} required/>
                        </Col>
                        <Col md={2}>
                            <button className={"createButtons mt-4"} type={"submit"}>TAYYOR</button>
                        </Col>
                    </Row>
                </Form>
            </Container>
            <div className={"mt-3"}>
                {delivery?.list?.length > 0 ? <div className={'tableCalendar'}>
                    <table>
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Manzil</th>
                            <th style={{minWidth: 100}}>Sana</th>
                            <th>Yetqazuvchi</th>
                            <th>Holati</th>
                            <th>Yuk xati</th>
                        </tr>
                        </thead>
                        {
                            delivery?.list?.map((product, index) =>
                                <tbody key={index}>
                                <tr style={{cursor: 'pointer'}}>
                                    <td>{index + 1}</td>
                                    <td onClick={() => onClickProduct(product)}>{product?.kindergartenNumber}{product?.kindergartenName}</td>
                                    <td>{TimestampToInputDate(product?.date)}</td>
                                    <td>{product?.deliver}</td>
                                    <td>{product?.status}</td>
                                    <td>
                                        <button className={"buttonPdf"} onClick={()=>dispatch(getCargoLetter(product?.id))}>Yuk xati</button>
                                    </td>
                                </tr>
                                {productReceived?.id === product?.id ? <tr className={"animate"}>
                                    <td colSpan={4}>
                                        <div className={"miniTable2"} style={{borderColor: 'red'}}>
                                            <table style={{backgroundColor: 'white'}}>
                                                <thead>
                                                <tr>
                                                    <th>№</th>
                                                    <th>Mahsulot nomi</th>
                                                    <th>Miqdori / Dona</th>
                                                    <th>Omborga kirim bo'lgan</th>
                                                    <th>Qabul qilish / Holati</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                {
                                                    product?.deliverySubDTOList?.map((item, index2) =>
                                                        <tr key={index2}>
                                                            <td>{index2 + 1}</td>
                                                            <td>{item.productName}</td>
                                                            <td>{item.packWeight}</td>
                                                            <td>{item?.warPackWeight}</td>
                                                            <td className={"d-flex justify-content-center align-items-center"}>
                                                                {product.status !== "YUBORILDI" && item?.restPackWeight > 0 ?
                                                                    <Form
                                                                        id={item.id}
                                                                        onSubmit={submitProductReceived(item, product)}>
                                                                        <InputGroup className="mb-3">
                                                                            <Form.Control
                                                                                type={"number"}
                                                                                name={"packWeight"}
                                                                                step={"0.01"}
                                                                                onWheel={e => e.target.blur()}
                                                                                max={item?.restPackWeight}
                                                                                style={{maxWidth: 110}}
                                                                                defaultValue={item?.restPackWeight}
                                                                                required
                                                                                min={0}
                                                                            />
                                                                            <InputGroup.Text id="basic-addon1">
                                                                                <button className={"buttonExcel"}
                                                                                        type={"submit"}
                                                                                        form={item.id}>Qabul qilish /
                                                                                    Holati
                                                                                </button>
                                                                            </InputGroup.Text>
                                                                        </InputGroup>
                                                                    </Form> :
                                                                    <span>{item?.status} : <span
                                                                        className={"fw-bolder"}>{item?.kinWeight}</span></span>}
                                                            </td>
                                                        </tr>
                                                    )
                                                }
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr> : null}
                                </tbody>
                            )
                        }
                    </table>
                    <br/>
                    <FromPageSizeBottom currentPage={delivery.getPageNumber}
                                        pageSize={delivery?.getPageSize} changesPage={changePage2}
                                        allPageSize={delivery?.allPageSize}/>
                </div> : delivery?.list ?
                    <div className={"text-center fs-3"} style={{color: 'red'}}>Ma'lumot mavjud emas </div> :
                    <div className={"text-center fs-3"} style={{color: 'red'}}>Qabul qilingan mahsulotlar mavjud
                        emas</div>}
            </div>
        </div>
    );
}

export default SentDelivery;
