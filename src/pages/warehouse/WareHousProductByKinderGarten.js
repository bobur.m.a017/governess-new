import React from 'react';
import {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import FromPageSizeBottom from "../fromPage/FromPageSizeBottom";
import {Col, Container, Form, InputGroup, Row} from "react-bootstrap";
import {getKindergartenWarehouse} from "./WarehouseReducer";
import {getFilesMttWarehouse, getGetFiles} from "../getFiles/GetFilesReducer";
import GetKinderByDepartment from "../GetKinderByDepartment";
import {TimestampToInputDate} from "../funcs/Funcs";

function WareHousProductByKinderGarten(props) {
    const [params, setParams] = useState({pageSize: 20, pageNumber: 0, kindergartenId: '', date: ''})
    const dispatch = useDispatch();
    const firstUpdate = useRef(false);
    const warehouses = useSelector(state => state.warehouse.kindergartenWarehouse);

    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
        }
    }, []);

    const getPdf = (e) => {
        if (params.kindergartenId) {
            dispatch(getFilesMttWarehouse({kindergartenId: params.kindergartenId}));
        }
    }

    const changePage0 = (pageNumber) => {
        dispatch(getKindergartenWarehouse({...params, pageNumber}));
        setParams({...params, pageNumber});
    }

    const getDepartmentId = (data) => {
    }

    const getKinderId = (data) => {
        dispatch(getKindergartenWarehouse({...params, kindergartenId: data?.id, pageNumber: 0}));
        setParams({...params, kindergartenId: data?.id});
    }
    return (
        <Container fluid={true}>
            {/*<NavbarHeader name={""}/>*/}
            <Row className={'mt-3'}>
                <Col>
                    <GetKinderByDepartment getDepartmentId={getDepartmentId} getKinderId={getKinderId}/>
                </Col>
                <Col className={'figma-card'}>
                    {warehouses?.list?.length > 0 ?
                        <>
                            <div className={'w-100 d-flex justify-content-end'}>
                                {/*<div>*/}
                                {/*    <InputGroup className="mb-3">*/}
                                {/*        <InputGroup.Text id="basic-addon1">Sana</InputGroup.Text>*/}
                                {/*        <Form.Control*/}
                                {/*            value={TimestampToInputDate(params.date)}*/}

                                {/*        />*/}
                                {/*    </InputGroup>*/}
                                {/*</div>*/}
                                <button className={'buttonExcel my-2'} onClick={getPdf}>Excel</button>
                            </div>
                            <div className={'tableCalendar'}>
                                <table>
                                    <thead>
                                    <tr>
                                        <th>№</th>
                                        <th>Mahsulot nomi</th>
                                        <th>Miqdor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        warehouses?.list?.map((product, index) =>
                                            <tr key={index} style={{cursor: 'pointer'}}>
                                                <td>{index + 1}</td>
                                                <td>{product.productName}</td>
                                                <td>{product.weight}</td>
                                            </tr>
                                        )
                                    }
                                    </tbody>
                                </table>
                                <br/>
                                <FromPageSizeBottom currentPage={warehouses?.getPageNumber}
                                                    pageSize={warehouses?.getPageSize} changesPage={changePage0}
                                                    allPageSize={warehouses?.allPageSize}/>
                            </div>
                        </>
                        : !warehouses?.list ? <div className={"text-center"}>Ma'lumotlar mavjud emas</div> :
                            <div className={"text-center"}>Omborda mahsulot mavjud emas</div>}
                </Col>

            </Row>
        </Container>
    );
}

export default WareHousProductByKinderGarten;
