import React from 'react';
import {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getMttFromSendProduct} from "../mtt/MttReducer";
import {Button, Col, Container, Form, Modal, Row} from "react-bootstrap";
import DistrictStyle from "../more/KindergartenStyle";
import KindergartenStyleAll from "../more/KindergartenStyleAll";
import {addDelivery, getSendProductByKinder} from "./WarehouseReducer";
import DropdownCustom from "../more/DropdownCustom";
import {MdOutlineDeleteOutline} from "react-icons/md";
import {toast} from "react-toastify";
import {TimestampToInputDate} from "../funcs/Funcs";
import {map} from "react-bootstrap/ElementChildren";

function SendProductTorKindergarten() {
    const def = {
        "date": '',
        "deliver": "",
        "kindergartenId": '',
        "productWeightResDTOList": [
            {
                "id": '',
                "pack": '',
                "packWeight": '',
                "productId": '',
                "productName": "",
                "weight": ''
            }
        ]
    }
    const [kindergartenId, setKindergartenId] = useState();
    const [fromGet, setFromGet] = useState([]);
    const [sendState, setSendState] = useState(def);
    const [fromAddProduct, setFromAddProduct] = useState([]);
    const dispatch = useDispatch();
    const mttsSendProduct = useSelector(state => state.mtt.mttsSendProduct);
    const result = useSelector(state => state.warehouse.result);
    const sendProductByKinder = useSelector(state => state.warehouse.sendProductByKinder);
    const error = useSelector(state => state.warehouse.error);
    const firstUpdate = useRef(false);

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    useEffect(() => {
        if (firstUpdate.current) {
            setFromGet(sendProductByKinder);
            setFromAddProduct(sendProductByKinder);
        }
    }, [sendProductByKinder]);

    useEffect(() => {
        if (firstUpdate.current) {
            dispatch(getMttFromSendProduct());
            dispatch(getSendProductByKinder(kindergartenId));
        }
    }, [result]);

    useEffect(() => {
        if (firstUpdate.current) {
            if (error?.response?.data?.data) {
                handleShow();
            }
        }
    }, [error]);


    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            dispatch(getMttFromSendProduct());
        }
    }, []);

    const getKinder = (data) => {
        dispatch(getSendProductByKinder(data.id));
        setKindergartenId(data.id);
    }
    const getProductFrom = (data) => {
        let list1 = [...fromGet];
        if (!fromGet.some(item => item.id === data.id)) {
            list1.push(data);
            setFromGet(list1);
        } else {
            toast.error("Bu mahsulot qo'shilgan");
        }
    }
    const deleteSendProd = (index) => {
        let list1 = [...fromGet];
        list1.splice(index, 1);
        setFromGet(list1);
    }
    const submitSendProduct = (e) => {
        e.preventDefault();
        dispatch(addDelivery({...sendState, productWeightResDTOList: fromGet, kindergartenId}))
    }

    const changeDate = (e) => {
        let value;
        if (e.target.name === "date") {
            value = new Date(e.target.value).getTime();
        } else {
            value = e.target.value;
        }
        setSendState({...sendState, [e.target.name]: value})
    }
    const changeWeight = (index) => (e) => {
        let weightList = [...fromGet];
        let pack = 1;
        if (weightList[index].pack > 1) {
            pack = weightList[index].pack;
        }
        let packWeight = e.target.value ? parseFloat(e.target.value) : '';
        let weight = packWeight * pack;
        weightList[index] = {...weightList[index], packWeight, weight}
        setFromGet(weightList);
    }
    return (
        <div>
            <Container fluid={true}>
                <Row className={"d-flex justify-content-between"}>
                    <Col sm={5} md={3} xl={3} lg={3} className={"figma-card-first"}>
                        <KindergartenStyleAll list={mttsSendProduct} subItemName={"sentProductSum"} subItem={true}
                                              getData={getKinder}/>
                    </Col>
                    <Col sm={7} md={8} xl={8} lg={8} className={"figma-card-first"}
                         style={{height: '70vh', overflowY: 'auto'}}>
                        <div className={"px-5 py-1"}>
                            <Form onSubmit={submitSendProduct}>
                                <div className={"d-flex justify-content-between w-100"}>
                                    <button className={"createButtons mb-3"} type={"submit"}>YUBORISH</button>
                                    <DropdownCustom list={fromAddProduct} name={"Mahsulot qo'shish"}
                                                    setData={getProductFrom} itemName={"productName"}/>
                                </div>
                                <div className={"d-flex justify-content-between w-100"}>
                                    <Form.Group className="shadow p-2 d-flex  justify-content-between"
                                    >
                                        <Form.Label column sm={4}>
                                            Sana
                                        </Form.Label>
                                        <Col sm={8}>
                                            <Form.Control type="date" placeholder=""
                                                          value={TimestampToInputDate(sendState.date)}
                                                          onWheel={e => e.target.blur()}
                                                          name={"date"} onChange={changeDate} required/>
                                        </Col>
                                    </Form.Group>
                                    <Form.Group className=" shadow p-2 d-flex  justify-content-between"
                                    >
                                        <Form.Label column sm={6}>
                                            Ta'minotchi ismi
                                        </Form.Label>
                                        <Col sm={6}>
                                            <Form.Control type="text" placeholder="" value={sendState.deliver}
                                                          onWheel={e => e.target.blur()}
                                                          name={"deliver"} onChange={changeDate} required/>
                                        </Col>
                                    </Form.Group>
                                </div>
                                {
                                    fromGet?.map((item, index) =>
                                        <Form.Group as={Row} className="p-2 d-flex  justify-content-between"
                                                    key={index}>
                                            <Form.Label column sm={7}>
                                                {item.productName}
                                            </Form.Label>
                                            <Col sm={4}>
                                                <Form.Control type="number" placeholder="" value={item.packWeight}
                                                              step={"0.001"} onWheel={e => e.target.blur()}
                                                              name={"packWeight"}
                                                              className={"w-100"} onChange={changeWeight(index)}
                                                              max={item?.maxWeight}
                                                              required/>
                                            </Col>
                                            <Col sm={1}>
                                                <button onClick={() => deleteSendProd(index)}
                                                        type={"button"}
                                                        style={{borderColor: 'white', backgroundColor: 'white'}}>
                                                    <MdOutlineDeleteOutline color={'red'} size={20}/></button>
                                            </Col>
                                        </Form.Group>
                                    )
                                }
                            </Form>
                        </div>
                    </Col>
                </Row>
            </Container>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title style={{color:'red'}}>Yetmayotgan mahsulotlar ro'yxati va miqdori</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className={"tableCalendar"}>
                        <table>
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Mahsulot nomi</th>
                                <th>Yetmayotgan miqdori</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                error?.response?.data?.data?.map((item,index)=>
                                    <tr key={index}>
                                        <td>{index+1}</td>
                                        <td>{item?.productName}</td>
                                        <td style={{color:'red'}}>{item?.packWeight}</td>
                                    </tr>
                                )
                            }
                            </tbody>
                        </table>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Yopish
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default SendProductTorKindergarten;
