import React from 'react';
import {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Col, Container, Form, Modal, Row} from "react-bootstrap";
import {
    addProductContract,
    addWarehouse,
    getAcceptedProduct,
    getAcceptedProductAll, getWareHoseExcel,
    getWarehouse, getWarehouseInout, getWarehousePdf
} from "./WarehouseReducer";
import NavbarHeader from "../more/NavbarHeader";
import FromPageSizeBottom from "../fromPage/FromPageSizeBottom";
import {TimestampToInputDate} from "../funcs/Funcs";
import MoreButtons from "../more/MoreButtons";
import {MdDeleteForever} from "react-icons/md";
import {getProduct} from "../product/ProductReducer";
import DropdownCustom from "../more/DropdownCustom";
import {getSupplier} from "../supplier/SupplierReducer";
import SendProductTorKindergarten from "./SendProductTorKindergarten";
import {toast} from "react-toastify";
import SentDelivery from "./SentDelivery";
import {getRoleStorage} from "../more/Functions";
import WareHousProductByKinderGarten from "./WareHousProductByKinderGarten";
import {addWasteFromWarehouse} from "../waste/WasteReducer";
import Waste from "../waste/Waste";

function Warehouse() {
    const defaults = {
        "id": "",
        "weight": "",
        name: "Mahsulotni tanlang ",
        price: "Mahsulotni tanlang "
    }
    const defaParams = {
        startDate: '',
        endDate: '',
        id: '',
        pageSize: 20,
        pageNumber: 0,
        supplierId: "",
        supplierName: "Ta'minotchini tanlang",
        productName: "Mahsulot tanlang",
        productId: null
    };
    const [wareHouseState, setWareHouseState] = useState();
    const [params, setParams] = useState(defaParams);
    const [productReceived, setProductReceived] = useState(defaults);
    const [currentNavs, setCurrentNavs] = useState(0);
    const [currentNumber, setCurrentNumber] = useState(0);
    const [supplier, setSupplier] = useState({name: "Ta'minotchini tanlang"});
    const [productState, setProductState] = useState();
    const [productCurrent, setProductCurrent] = useState();
    const [inoutList, setInoutList] = useState();
    const [inoutState, setInoutState] = useState();
    const [inoutData, setInoutData] = useState();
    const warehouses = useSelector(state => state.warehouse.warehouses);
    const mttsSendProduct = useSelector(state => state.mtt.mttsSendProduct);
    const result = useSelector(state => state.warehouse.result);
    const wasteResult = useSelector(state => state.waste.result);
    const products = useSelector(state => state.product.products);
    const suppliers = useSelector(state => state.supplier.suppliers);
    const acceptedProduct = useSelector(state => state.warehouse.acceptedProduct);
    const sendWarehouseIonut = useSelector(state => state.warehouse.sendWarehouseIonut);
    const dispatch = useDispatch();
    const firstUpdate = useRef(false);
    const [show2, setShow2] = useState(false);
    const [show, setShow] = useState(false);

    const handleClose = () => {
        setInoutList([]);
        setInoutState({});
        setShow(false);
    }
    const handleClose2 = () => {
        setParams(defaParams);
        setShow2(false)
    };
    const handleShow = (num) => {
        setCurrentNumber(num);
        setShow(true);
    }
    const handleShow2 = (id) => {
        dispatch(getProduct());
        dispatch(getWarehouseInout({...params, id, pageNumber: 0, productName: "Mahsulot tanlang", productId: null}));
        setParams({...params, id, pageNumber: 0});
        setShow2(true);
    }
    useEffect(() => {
        if (!firstUpdate.current) {

        } else {
            dispatch(getAcceptedProductAll());
            dispatch(getWarehouse());
            setProductReceived(defaults);
            handleClose();
        }
    }, [result, wasteResult]);


    useEffect(() => {
        if (firstUpdate.current) {
        }
    }, [mttsSendProduct]);

    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            dispatch(getAcceptedProductAll());
            dispatch(getWarehouse({pageNumber: 0, pageSize: 20}));
            dispatch(getSupplier());
            dispatch(getProduct());
        } else {
            setWareHouseState(warehouses);
        }
    }, [acceptedProduct]);

    const onClickProduct = (data) => {
        setProductReceived({...data, packWeight: '', date: Date.now()});
        handleShow(1);
    }

    const submit = (e) => {
        e.preventDefault();
        dispatch(addProductContract(productReceived));
    }

    const onChangeProductWeight = (e) => {
        setProductReceived({...productReceived, [e.target.name]: e.target.value});
    }

    const getPdf = () => {
        dispatch(getWarehousePdf(params));
    }
    const changePage1 = (page) => {
        dispatch(getAcceptedProductAll({page, pageSize: 20}));
    }
    const changePage0 = (pageNumber) => {
        dispatch(getWarehouse({pageNumber, pageSize: 20}));
    }
    const changePage2 = (page) => {
        dispatch(getAcceptedProduct({page, pageSize: 20}));
    }
    const changePage3 = (pageNumber) => {
        dispatch(getWarehouseInout({...params, pageNumber}));
        setParams({...params, pageNumber});
    }
    const activeOrInActive = (data) => {
        setProductState(data);
    }
    const getDateMore = (index, data) => {
        setProductState(data);
        setProductCurrent(data);
        handleShow(2)
    }
    const inputProduct = () => {
        return (
            <Form onSubmit={submit}>
                <Modal.Header closeButton>
                    <Modal.Title>{productReceived?.productName}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                        <span className={'mb-3'} style={{color: '#fcb713'}}>Maximal kiritish miqdori:
                            <span style={{color: '#000'}}>{productReceived?.residualWeight}</span></span>
                    <br/>
                    <Form.Label>Sana</Form.Label>
                    <Form.Control type={'date'} name={"date"}
                                  value={TimestampToInputDate(productReceived.date)}
                                  onChange={onChangeProductWeight}
                                  onWheel={event => event.target.blur()}
                                  max={TimestampToInputDate(productReceived.date)}/>
                    <Form.Label>Qabul qilinadigan miqdor</Form.Label>
                    <Form.Control max={productReceived?.residualPackWeight} type={'number'} name={"packWeight"}
                                  value={productReceived.packWeight} step={'0.001'}
                                  onChange={onChangeProductWeight}
                                  onWheel={event => event.target.blur()} required/>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Bekor qilish
                    </Button>
                    <Button variant="primary" type={'submit'}>
                        Tayyor
                    </Button>
                </Modal.Footer>
            </Form>
        )
    }
    const submitWaste = (e) => {
        e.preventDefault();
        dispatch(addWasteFromWarehouse({inOutPriceId: inoutState?.inOutPriceId, weight: inoutState?.weight}))
    }
    const getProductData = (data) => {
        setProductReceived({...productReceived, productId: data.id, name: data.name})
    }
    const submitInoutGet = (e) => {
        e.preventDefault();
        dispatch(getWarehouseInout(params));
    }
    const wasteProduct = () => {
        return (
            <div className={"tableCalendar"}>
                <Modal.Header></Modal.Header>
                <Modal.Body>
                    <table>
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Narxi</th>
                            <th>Miqdori</th>
                            {getRoleStorage() === "ROLE_OMBOR_MUDIRI" ? <th>Tanlash</th> : null}
                        </tr>
                        </thead>
                        <tbody>
                        {
                            inoutList?.map((item, index) =>
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td>{item.price}</td>
                                    <td>
                                        <Form onSubmit={submitWaste} className={"d-flex justify-content-between"}>
                                            <input type="number" step={"0.01"} max={item.weight}
                                                   onChange={e => setInoutState({
                                                       ...inoutState,
                                                       inOutPriceId: item?.id,
                                                       weight: e.target.value,
                                                       id: item?.id
                                                   })} defaultValue={item.weight}
                                                   disabled={!(item?.id === inoutState?.id)}/>
                                            {(item?.id === inoutState?.id) ? <button className={"buttonExcel"}
                                                                                     type={"submit"}>Tayyor</button> : null}
                                        </Form>
                                    </td>
                                    {getRoleStorage() === "ROLE_OMBOR_MUDIRI" ? <td>
                                        {(item?.id === inoutState?.id) ? null :
                                            <button className={"buttonPdf"} onClick={() => setInoutState(item)}>Chiqidga
                                            </button>}
                                    </td> : null}
                                </tr>
                            )
                        }
                        </tbody>
                    </table>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant={"danger"} onClick={handleClose}>Ortga</Button>
                </Modal.Footer>
            </div>
        )
    }
    const receivedProduct = () => {
        handleShow(3);
    }
    const submitReceived = (e) => {
        e.preventDefault();
        if (supplier?.id && productReceived?.productId) {
            dispatch(addWarehouse({
                productId: productReceived?.productId,
                weight: productReceived?.weight,
                price: productReceived?.price
            }, supplier?.id));

        } else {
            toast.error("Barcha ma'lumotlar tanlanmagan!")
        }
    }
    const renderReceivedProduct = () => {
        return (
            <Form onSubmit={submitReceived}>
                <Modal.Header closeButton>
                    <Modal.Title>
                        <div>
                            <DropdownCustom name={productReceived?.name} setData={getProductData}
                                            list={products}/>
                        </div>
                        <div>
                            <DropdownCustom name={supplier?.name} setData={setSupplier}
                                            list={suppliers?.list}/>
                        </div>
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Label>Qabul qilinadigan miqdor</Form.Label>
                    <Form.Control type={'number'} name={"weight"}
                                  value={productReceived?.weight} step={'0.001'}
                                  onChange={onChangeProductWeight}
                                  onWheel={event => event.target.blur()} required/>
                    <Form.Label>Mahsulot narxi</Form.Label>
                    <Form.Control type={'number'} name={"price"}
                                  value={productReceived?.price} step={'0.001'}
                                  onChange={onChangeProductWeight}
                                  onWheel={event => event.target.blur()} required/>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Bekor qilish
                    </Button>
                    <Button variant="primary" type={'submit'}>
                        Tayyor
                    </Button>
                </Modal.Footer>
            </Form>
        )
    }
    const supplierGEt = (data) => {
        setParams({...params, supplierId: data.id, supplierName: data.name})
    }
    const getProductDrop = (data) => {
        setParams({...params, productId: data.id, productName: data.name})
    }
    const inoutListRender = (data) => {
        setInoutList(data?.inOutList);
        setInoutData(data);
        handleShow(2);
    }

    return (
        <div className={"allMain"}>
            <NavbarHeader name={"Omborxona"}
                          navs={[{name: "Ombordagi mahsulotlar"},
                              {name: "Yuborilgan mahsulotlar"},
                              getRoleStorage() === "ROLE_OMBOR_MUDIRI" ? {name: "Mahsulotni yuborish"} : {name: "MTT qoldiqlari"},
                              {name: "Chiqidlar"}]}
                          currentNavs={setCurrentNavs}/>
            <Container fluid={true} className={'mt-3'}>
                {currentNavs === 0 ? <Row>
                    {getRoleStorage() === "ROLE_OMBOR_MUDIRI" ? <div>
                        <button className={"createButtons mb-3"} onClick={receivedProduct}>
                            Mahsulot qabul qilish
                        </button>
                    </div> : null}
                    <div>
                        <div className={"d-flex justify-content-center"}>
                            <button className={"createButtons mb-3 mx-3"} onClick={() => handleShow2()}>
                                Kirim
                            </button>
                            <button className={"buttonExcel mb-3 mx-3"} onClick={() => dispatch(getWareHoseExcel())}>
                                Excel
                            </button>
                        </div>
                    </div>
                    {warehouses?.list?.length > 0 ? <Col className={'figma-card'}>
                        <div className={'w-100 d-flex justify-content-end'}>
                            {/*<button className={'buttonPdf my-2'} onClick={getPdf}>PDF</button>*/}
                        </div>
                        <div className={'tableCalendar'}>
                            <table>
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Mahsulot nomi</th>
                                    <th>Miqdor</th>
                                    <th>Qadoqlar soni</th>
                                    {/*<th>Chiqid</th>*/}
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    warehouses?.list?.map((product, index) =>
                                        <tr key={index}
                                        >
                                            <td>{index + 1}</td>
                                            <td style={{cursor: 'pointer'}}
                                                onClick={() => inoutListRender(product)}>{product.productName}</td>
                                            <td style={{cursor: 'pointer'}}
                                                onClick={() => inoutListRender(product)}>{product.weight}</td>
                                            <td style={{cursor: 'pointer'}}
                                                onClick={() => inoutListRender(product)}>{product.packWeight}</td>
                                            {/*<td><MoreButtons list={[*/}
                                            {/*    {name: "Chiqidga chiqarish", icon: <MdDeleteForever/>}*/}
                                            {/*]} data={product} setActive={activeOrInActive}*/}
                                            {/*                 active={productState?.id === product.id}*/}
                                            {/*                 getDate={getDateMore}/></td>*/}
                                        </tr>
                                    )
                                }
                                </tbody>
                            </table>
                            <br/>
                            <FromPageSizeBottom currentPage={warehouses.getPageNumber}
                                                pageSize={warehouses?.getPageSize} changesPage={changePage0}
                                                allPageSize={warehouses?.allPageSize}/>
                        </div>
                    </Col> : warehouses?.list ?
                        <div className={"text-center fs-3"} style={{color: 'red'}}>Ma'lumot mavjud emas </div> :
                        <div className={"text-center fs-3"} style={{color: 'red'}}>Qabul qilingan mahsulotlar mavjud
                            emas</div>}
                </Row> : null}
                {currentNavs === 1 ? <Row>
                    <Col className={'figma-card'}>
                        <SentDelivery/>
                    </Col>
                </Row> : null}
                {currentNavs === 2 ? getRoleStorage() === "ROLE_OMBOR_MUDIRI" ? <SendProductTorKindergarten/> :
                    <WareHousProductByKinderGarten/> : null}
                {currentNavs === 3 ? <Waste/> : null}
            </Container>
            <Modal show={show} onHide={handleClose}>
                {currentNumber === 1 ? inputProduct() : null}
                {currentNumber === 2 ? wasteProduct() : null}
                {currentNumber === 3 ? renderReceivedProduct() : null}
            </Modal>
            <Modal show={show2} onHide={handleClose2} size={"xl"}>
                <Modal.Header closeButton>
                    <Form onSubmit={submitInoutGet} className={"w-100"}>
                        <Row className={"w-100"}>
                            <Col md={2}>
                                <Form.Label>Boshlanish sana</Form.Label>
                                <Form.Control type={'date'} name={"startDate"}
                                              value={TimestampToInputDate(params.startDate)}
                                              onChange={(e) => setParams({
                                                  ...params,
                                                  [e.target.name]: new Date(e.target.value).getTime()
                                              })}/>
                            </Col>
                            <Col md={2}>
                                <Form.Label>Tugash sana</Form.Label>
                                <Form.Control type={'date'} name={"endDate"}
                                              value={TimestampToInputDate(params.endDate)}
                                              min={TimestampToInputDate(params.startDate)}
                                              onChange={(e) => setParams({
                                                  ...params,
                                                  [e.target.name]: new Date(e.target.value).getTime()
                                              })}
                                />
                            </Col>
                            <Col md={3}>
                                <br/>
                                <DropdownCustom list={suppliers?.list} name={params.supplierName}
                                                setData={supplierGEt}/>
                            </Col>
                            <Col md={3}>
                                <br/>
                                <DropdownCustom name={params.productName} list={products} setData={getProductDrop}/>
                            </Col>
                            <Col md={2}>
                                <button className={"buttonPdf mt-3"} type={"button"} onClick={() => getPdf()}>PDF
                                </button>
                                <br/>
                                <button className={"createButtons mt-3"} type={"submit"}>TAYYOR</button>
                            </Col>
                        </Row>
                    </Form>
                </Modal.Header>
                <Modal.Body>
                    <div className={'tableCalendar'}>
                        <table>
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Nomi</th>
                                <th>Narxi</th>
                                <th>Qabul miqdori</th>
                                <th>Qadog'i</th>
                                <th>Qabul qilingan sana</th>
                                {/*<th>Ombor qoldig'gi (sana asosida)</th>*/}
                                <th>Yetqazuvchi</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                sendWarehouseIonut?.list?.map((product, index) =>
                                    <tr key={index} style={{cursor: 'pointer'}}>
                                        <td>{index + 1}</td>
                                        <td>{product.productName}</td>
                                        <td>{product.price} so'm</td>
                                        <td>{product.packWeight}</td>
                                        <td>{product.pack}</td>
                                        <td>{TimestampToInputDate(product?.createDate)}</td>
                                        {/*<td>{product?.restByDate}</td>*/}
                                        <td>{product?.sender}</td>
                                    </tr>
                                )
                            }
                            </tbody>
                        </table>
                        <br/>
                        <FromPageSizeBottom currentPage={sendWarehouseIonut?.getPageNumber}
                                            pageSize={sendWarehouseIonut?.getPageSize} changesPage={changePage3}
                                            allPageSize={sendWarehouseIonut?.allPageSize}/>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Bekor qilish
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default Warehouse;
