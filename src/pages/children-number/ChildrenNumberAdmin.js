import React from 'react';
import {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getKidsNumbersAdmin} from "./ChildrenNumberReducer";
import {Col, Form, Row} from "react-bootstrap";
import {TimestampToInputDate} from "../funcs/Funcs";
import DropdownCustom from "../more/DropdownCustom";
import {getWarehouseInout} from "../warehouse/WarehouseReducer";

function ChildrenNumberAdmin(props) {
    const defaParams = {
        startDate: '',
        endDate: '',
        id: '',
        pageSize: 20,
        pageNumber: 0,
        supplierId: "",
        supplierName: "Ta'minotchini tanlang"
        ,productName:"Mahsulot tanlang",
        productId:null
    };
    const faceAge = [{name:"3-4 yosh"},{name:"4-7"},{name:"qisqa muddatli"}];
    const faceMTT = [{name:"MTT-",number:3},{name:"MTT-",number:3},{name:"MTT-",number:3}];
    const [ageState, setAgeState] = useState(faceAge);
    const [mttState, setMttState] = useState(faceMTT);
    const dispatch = useDispatch();
    const stateSelector = useSelector(state => state.kidsNumber.kidsNumbersAdmin);
    const [params, setParams] = useState(defaParams);
    const firstUpdate = useRef(false);

    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            // dispatch(getKidsNumbersAdmin());
        }
    }, []);
    const submitInoutGet = (e) => {
        e.preventDefault();
        dispatch(getWarehouseInout(params));
    }
    const getDataDistrict = (data) => {

    }
    return (
        <div className={"figma-card mt-3"}>
            <div>
                <Form onSubmit={submitInoutGet} className={"w-100"}>
                    <Row className={"w-100"}>
                        <Col md={2}>
                            <Form.Label>Boshlanish sana</Form.Label>
                            <Form.Control type={'date'} name={"startDate"}
                                          value={TimestampToInputDate(params.startDate)}
                                          onChange={(e) => setParams({
                                              ...params,
                                              [e.target.name]: new Date(e.target.value).getTime()
                                          })}/>
                        </Col>
                        <Col md={2}>
                            <Form.Label>Tugash sana</Form.Label>
                            <Form.Control type={'date'} name={"endDate"}
                                          value={TimestampToInputDate(params.endDate)}
                                          min={TimestampToInputDate(params.startDate)}
                                          onChange={(e) => setParams({
                                              ...params,
                                              [e.target.name]: new Date(e.target.value).getTime()
                                          })}
                            />
                        </Col>
                        <Col md={3}>
                            <br/>
                            {/*<DropdownCustom list={suppliers?.list} name={params.supplierName}*/}
                            {/*                setData={getDataDistrict}/>*/}
                        </Col>
                        <Col md={2}>
                            <br/>
                            <button className={"createButtons mt-3"} type={"submit"}>TAYYOR</button>
                        </Col>
                    </Row>
                </Form>
            </div>
            <div className={"tableCalendar"}>
                <table>
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>MTT</th>
                        {
                            ageState.map((item,index)=>
                            <th key={index}>{item.name}</th>
                            )
                        }
                        <th>Holati</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        mttState.map((mtt,index)=>
                        <tr key={index}>
                            <td>{index+1}</td>
                            <td>{mtt.name}</td>
                            {
                                ageState.map((item,index)=>
                                    <td key={index}>{item.name}</td>
                                )
                            }
                            <td>YANGI</td>
                        </tr>
                        )
                    }
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default ChildrenNumberAdmin;