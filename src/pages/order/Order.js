import React from 'react';
import {useEffect, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Button, Col, Form, Modal, Row} from "react-bootstrap";
import {addOrder, deleteOrder, editOrder, getOrder, sendOrder} from "./OrderReducer";
import NavbarHeader from "../more/NavbarHeader";
import More from "../more/DropdownCustom";
import MoreButtons from "../more/MoreButtons";
import {AiFillDelete, AiFillEdit} from "react-icons/ai";
import {TimestampToInputDate} from "../funcs/Funcs";
import {useNavigate} from "react-router-dom";
import FromPageSizeBottom from "../fromPage/FromPageSizeBottom";

function Order() {
    const history = useNavigate();
    const orderDef = {
        name: "",
        start: "",
        end: "",
        date: '',
    };
    const [orderState, setOrderState] = useState(orderDef);
    const [active, setActive] = useState();
    const [number, setNumber] = useState(0);
    const [params, setParams] = useState({pageSize: 20, page: 0});
    const dispatch = useDispatch();
    const order = useSelector(state => state.order)
    const firstUpdate = useRef(false);
    const [show, setShow] = useState(false);
    const handleClose = () => {
        setOrderState({});
        setShow(false)
    };
    const handleShow = (data) => {
        if (data === null) {
            setOrderState(orderDef);
            setNumber(0);
        } else {
            setNumber(1);
            setOrderState(data);
        }
        setShow(true)
    };

    useEffect(() => {
        if (firstUpdate.current) {
            handleClose();
            dispatch(getOrder(params));
        }
    }, [order.result]);

    useEffect(() => {
        if (!firstUpdate.current) {
            firstUpdate.current = true;
            dispatch(getOrder(params));
        }
    }, []);
    const onChangeOrder = (e) => {
        if (e.target.name === 'name') {
            setOrderState({...orderState, [e.target.name]: e.target.value});
        } else {
            setOrderState({...orderState, [e.target.name]: new Date(e.target.value).getTime()});
        }
    }
    const submitCreateOrder = (e) => {
        e.preventDefault();
        if (orderState?.id) {
            dispatch(editOrder(orderState));
        } else {
            dispatch(addOrder(orderState));
        }
        handleClose();
    }
    const setOrderData = (index, data) => {
        setOrderState(data);
        if (index === 0) {
            setNumber(0);
            handleShow(null);
        } else if (index === 1) {
            dispatch(deleteOrder(data))
        }
    }
    const activeMore = (data) => {
        history("/sidebar/order/" + data?.id);
        setActive(data);
    }
    const changePage = (page) => {
        let params2 = {...params, page}
        console.log(params2);
        dispatch(getOrder(params2));
        setParams(params2);
    }
    const renderFunc = () => {
        if (number === 0) {
            return (
                <Form onSubmit={submitCreateOrder}>
                    <Modal.Header closeButton>
                        <Modal.Title>Buyurtmani tuzish</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group as={Row} className="mb-3" controlId="formPlaintextPassword">
                            <Form.Label column sm="4">
                                Nomi
                            </Form.Label>
                            <Col sm="8">
                                <Form.Control type="text" placeholder="Nomi" size={"sm"} value={orderState?.name}
                                              required
                                              name={"name"} onChange={onChangeOrder}/>
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} className="mb-3" controlId="formPlaintextPassword">
                            <Form.Label column sm="4">
                                Boshlanish
                            </Form.Label>
                            <Col sm="8">
                                <Form.Control type="date" size={"sm"} value={TimestampToInputDate(orderState?.start)}
                                              name={"start"} onChange={onChangeOrder}/>
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} className="mb-3" controlId="formPlaintextPassword">
                            <Form.Label column sm="4">
                                Tugash
                            </Form.Label>
                            <Col sm="8">
                                <Form.Control type="date" size={"sm"} value={TimestampToInputDate(orderState?.end)}
                                              name={"end"} onChange={onChangeOrder}/>
                            </Col>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" type={"button"} onClick={handleClose}>
                            Ortga
                        </Button>
                        <Button variant="primary" type={"submit"}>
                            Tayyor
                        </Button>
                    </Modal.Footer>
                </Form>
            )
        } else if (number === 1) {
            return (
                <Form onSubmit={sendOrderFunc}>
                    <Modal.Header>
                    <div style={{color: "orange"}} className={"fs-3"}>
                        Yuborilgan zayavkani qayta tahrirlab bo'lmaydi!
                        Yuborasizmi!
                    </div>
                    </Modal.Header>
                    <Modal.Footer>
                        <Button variant="secondary" type={"button"} onClick={handleClose}>
                            YO'Q
                        </Button>
                        <Button variant="primary" type={"submit"}>
                            HA
                        </Button>
                    </Modal.Footer>
                </Form>
            )
        }
        return null;
    }
    const sendOrderFunc = (e) => {
        e.preventDefault();
        dispatch(sendOrder(orderState?.id))
    }

    return (
        <div>
            <NavbarHeader name={"Buyurtmalar"} handleShow={handleShow} buttonName={"Buyurtma tuzish"}/>
            <div className={"figma-card mt-3"}>
                <div className={"tableCalendar"}>
                    <table>
                        <thead>
                        <tr>
                            <th>№</th>
                            <th>Nomi</th>
                            <th>Sana</th>
                            <th>Yuborish</th>
                            <th>Boshqa</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            order?.orders?.list?.length > 0 ? order?.orders?.list?.map((item, index) =>
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td onClick={() => activeMore(item)} style={{cursor: 'pointer'}}>{item.name}</td>
                                    <td>{TimestampToInputDate(item?.updateDate)}</td>
                                    <td>
                                        {!item?.status ? <button className={"buttonExcel"} onClick={() => handleShow(item)}>Yuborish
                                        </button>:item?.status}
                                    </td>
                                    <td>
                                        <MoreButtons list={[
                                            {name: "O'zgartirish", icon: <AiFillEdit size={20}/>},
                                            {name: "O'chirish", icon: <AiFillDelete size={20}/>}]}
                                                     data={item} active={active?.id === item?.id}
                                                     setActive={setActive}
                                                     getDate={setOrderData}
                                        />
                                    </td>
                                </tr>
                            ) : <div className={"fs-3 text-center w-100"} style={{color: 'red'}}>Ma'lumot mavjud
                                emas</div>
                        }
                        </tbody>
                    </table>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <FromPageSizeBottom pageSize={order?.orders?.getPageSize} currentPage={order?.orders?.getPageNumber}
                                        changesPage={changePage} allPageSize={order?.orders?.allPageSize}/>
                </div>
            </div>
            <Modal show={show} onHide={handleClose}>
                {renderFunc()}
            </Modal>

        </div>
    );
}

export default Order;
